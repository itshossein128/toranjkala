<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-grey">
<?php include 'header.php'; ?>
<div class="breadcrumb-container">
    <nav class="container" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">صفحه اصلی</a></li>
            <li class="breadcrumb-item"><a href="#">فروشگاه</a></li>
            <li class="breadcrumb-item"><a href="#">لوازم منزل و فانتزی</a></li>
            <li class="breadcrumb-item"><a href="#">لوازم شخصی</a></li>
            <li class="breadcrumb-item active" aria-current="page">هندزفری پلاتینا مدل</li>
        </ol>
    </nav>
</div>
<main>
    <div class="container banner-container position-relative">
        <img data-src="assets/images/banner-bg.png" class="img-fluid js-lazy d-none d-md-block">
        <img data-src="assets/images/banner-bg-mobile.svg" class="img-fluid js-lazy d-block d-md-none w-100">
        <img data-src="assets/images/iphone_13_PNG33.png" class="banner-right-side-img js-lazy ">
        <img data-src="assets/images/iPhone-13-PNG-Photos.png" class="banner-left-side-img js-lazy d-none d-md-block">
        <div class="banner-content">
            <div class="banner-titre">
                <span>جدیدترین</span>
                <span>تلفن‌های هوشمند</span>
                <button>
                    همین حالا خرید کنید
                    <img data-src="assets/images/slider-button-icon.svg" class="js-lazy">
                </button>
            </div>
        </div>
    </div>
    <div class="carts-with-shadow-container">
        <div>
            <section class="container-sm instant-offer-section">
                <div class="d-flex justify-content-between align-items-center instant-offer__heading-container">
        <span class="instant-offer__heading"><img data-src="assets/images/instant-offer.svg"
                                                  class="me-2 js-lazy"> پیشنهادات لحظه‌ای</span>
                    <div class="d-flex justify-content-between align-items-center mobile-size-slider-buttons-container">
                        <div class="instant-offer__button-next d-block d-sm-none"><img data-src="assets/images/angel-prev.svg"
                                                                                       class="angel-next js-lazy">
                        </div>
                        <div class="instant-offer__button-prev d-flex justify-content-end align-items-center align-self-stretch d-sm-none">
                            <img data-src="assets/images/angel-prev.svg" class="js-lazy">
                        </div>
                    </div>
                </div>
                <div class="instant-offer-container dir-ltr">
                    <div class="instant-offer__button-prev d-none d-sm-block"><img src="assets/images/category-prev.svg" alt="">
                    </div>
                    <div class="instant-offer__button-next d-none d-sm-block"><img src="assets/images/category-next.svg" alt="">
                    </div>
                    <div class="main-slider instant-offer swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide my-cart">
                                <a href="#">
                                    <div class="my-cart__off">۲۰٪</div>
                                    <div class="my-cart__img-container">
                                        <img data-src="assets/images/slide1img.png" class="img-fluid js-lazy">
                                    </div>
                                    <div class="my-cart__footer">
                                        <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                                        <div class="my-cart__footer__price">
                                            <span>۷۱,۸۰۰</span>
                                            <span>تومن</span>
                                            <del>۱۱۰.۰۰۰</del>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide my-cart off-active">
                                <a href="#">
                                    <div class="my-cart__off">۲۰٪</div>
                                    <div class="my-cart__img-container">
                                        <img data-src="assets/images/slide2img.png" class="img-fluid js-lazy">
                                    </div>
                                    <div class="my-cart__footer">
                                        <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                                        <div class="my-cart__footer__price">
                                            <span>۷۱,۸۰۰</span>
                                            <span>تومن</span>
                                            <del>۱۱۰.۰۰۰</del>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide my-cart">
                                <a href="#">
                                    <div class="my-cart__off">۲۰٪</div>
                                    <div class="my-cart__img-container">
                                        <img data-src="assets/images/slide3img.png" class="img-fluid js-lazy">
                                    </div>
                                    <div class="my-cart__footer">
                                        <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                                        <div class="my-cart__footer__price">
                                            <span>۷۱,۸۰۰</span>
                                            <span>تومن</span>
                                            <del>۱۱۰.۰۰۰</del>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide my-cart">
                                <a href="#">
                                    <div class="my-cart__off">۲۰٪</div>
                                    <div class="my-cart__img-container">
                                        <img data-src="assets/images/slide4img.png" class="img-fluid js-lazy">
                                    </div>
                                    <div class="my-cart__footer">
                                        <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                                        <div class="my-cart__footer__price">
                                            <span>۷۱,۸۰۰</span>
                                            <span>تومن</span>
                                            <del>۱۱۰.۰۰۰</del>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide my-cart">
                                <a href="#">
                                    <div class="my-cart__off">۲۰٪</div>
                                    <div class="my-cart__img-container">
                                        <img data-src="assets/images/slide1img.png" class="img-fluid js-lazy">
                                    </div>
                                    <div class="my-cart__footer">
                                        <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                                        <div class="my-cart__footer__price">
                                            <span>۷۱,۸۰۰</span>
                                            <span>تومن</span>
                                            <del>۱۱۰.۰۰۰</del>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide my-cart">
                                <a href="#">
                                    <div class="my-cart__off">۲۰٪</div>
                                    <div class="my-cart__img-container">
                                        <img data-src="assets/images/slide1img.png" class="img-fluid js-lazy">
                                    </div>
                                    <div class="my-cart__footer">
                                        <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                                        <div class="my-cart__footer__price">
                                            <span>۷۱,۸۰۰</span>
                                            <span>تومن</span>
                                            <del>۱۱۰.۰۰۰</del>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="instant-offer-pagination dir-ltr d-flex justify-content-center"></div>
                </div>
            </section>
        </div>
    </div>
    <div class="container d-flex flex-column flex-md-row align-items-start justify-content-between shop-main-content ">
        <aside class="shop-filters-container">
            <div class="shop-filter__category">
                <div class="shop-filter__heading">
                    دسته‌بندی‌ها
                </div>
                <div class="shop-filter-categories-container">
                    <div class="shop-filter__category-closed">
                        <button>
                            <span>لوازم جانبی</span>
                            <span class="shop-filter__number-of-products">۳۲</span>
                            <div class="shop-filter-minus"></div>
                        </button>
                        <ul>
                            <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                            <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                            <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
                        </ul>
                    </div>
                    <div class="shop-filter__category-closed">
                        <button>
                            <span>لوازم جانبی</span>
                            <span class="shop-filter__number-of-products">۳۲</span>
                            <div class="shop-filter-minus"></div>
                        </button>
                        <ul>
                            <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                            <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                            <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
                        </ul>
                    </div>
                    <div class="shop-filter__category-closed">
                        <button>
                            <span>لوازم جانبی</span>
                            <span class="shop-filter__number-of-products">۳۲</span>
                            <div class="shop-filter-minus"></div>
                        </button>
                        <ul>
                            <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                            <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                            <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
                        </ul>
                    </div>
                    <div class="shop-filter__category-closed">
                        <button>
                            <span>لوازم جانبی</span>
                            <span class="shop-filter__number-of-products">۳۲</span>
                            <div class="shop-filter-minus"></div>
                        </button>
                        <ul>
                            <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                            <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                            <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
                        </ul>
                    </div>
                    <div class="shop-filter__category-closed">
                        <button>
                            <span>لوازم جانبی</span>
                            <span class="shop-filter__number-of-products">۳۲</span>
                            <div class="shop-filter-minus"></div>
                        </button>
                        <ul>
                            <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                            <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                            <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="shop-filter__price-range">
                <div class="shop-filter__heading">
                    محدوده قیمت
                </div>
                <div class="shop-filter__range-slider-container">
                    <div id="RangeSlider" class="range-slider dir-ltr" data-min-toman="0" data-max-toman="1000000">
                        <div>
                            <div class="range-slider-val-left"></div>
                            <div class="range-slider-val-right"></div>
                            <div class="range-slider-val-range"></div>

                            <span class="range-slider-handle range-slider-handle-left"></span>
                            <span class="range-slider-handle range-slider-handle-right"></span>


                        </div>

                        <input type="range" class="range-slider-input-left" tabindex="0" max="100" min="0" step="1">
                        <input type="range" class="range-slider-input-right" tabindex="0" max="100" min="0" step="1">
                    </div>
                    <div class="shop-filter__range-slider__props">
                        قیمت از:
                        <span class="range-slider-tooltip-left"></span>
                        تومان تا
                        <span class="range-slider-tooltip-right"></span>
                        تومان
                    </div>
                </div>
            </div>
            <div class="shop-filter-brands">
                <div class="shop-filter__heading">
                    برندها
                </div>
                <ul>
                    <li>
                        <label>
                            <div class="d-flex align-items-center">
                                <input type="checkbox">
                                <div></div>
                                اپل
                            </div>
                            <span>۴۵</span>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div class="d-flex align-items-center">
                                <input type="checkbox">
                                <div></div>
                                سامسونگ
                            </div>
                            <span>۱۶</span>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div class="d-flex align-items-center">
                                <input type="checkbox">
                                <div></div>
                                هایلو
                            </div>
                            <span>۹</span>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div class="d-flex align-items-center">
                                <input type="checkbox">
                                <div></div>
                                شیائومی
                            </div>
                            <span>۲۳</span>
                        </label>
                    </li>
                </ul>
            </div>
        </aside>
        <section class="shop-products-container">
            <div class="shop__selects-container">
                <div class="custom-select-container">
                    <div class="custom-select">
                        <select>
                            <option value="1">نمایش ۲۰ محصول</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                </div>
                <div class="custom-select-container">
                    <div class="custom-select">
                        <select>
                            <option value="1">مرتب‌سازی پیشفرض</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="shop-carts-container">
                <div class="my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide2img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide1img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide3img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide2img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide1img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide3img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide2img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide1img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide3img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="shop__show-more-carts">
                    <button class="btn border-0 bg-transparent">
                        مشاهده بیشتر
                        <img data-src="assets/images/shop-show-more-button.svg" class="ms-2 js-lazy">
                    </button>
                </div>
            </div>
        </section>
    </div>
</main>
<?php include 'footer.php'; ?>
</body>
</html>