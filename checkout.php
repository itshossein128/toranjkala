<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-grey">
<?php include 'header.php'; ?>
<div class="breadcrumb-container">
    <nav class="container" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">صفحه اصلی</a></li>
            <li class="breadcrumb-item"><a href="#">فروشگاه</a></li>
            <li class="breadcrumb-item"><a href="#">لوازم منزل و فانتزی</a></li>
            <li class="breadcrumb-item"><a href="#">لوازم شخصی</a></li>
            <li class="breadcrumb-item active" aria-current="page">هندزفری پلاتینا مدل</li>
        </ol>
    </nav>
</div>
<div class="d-flex flex-column flex-lg-row justify-content-between align-items-start container">
    <section class="checkout__main-section">
        <div class="checkout__main-section__heading">
            اطلاعات شما
        </div>
        <form class="row g-3">
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">نام <img data-src="assets/images/form-important-star.svg" class="js-lazy"></label>
                <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="col-md-6">
                <label for="inputPassword4" class="form-label">نام خانوادگی <img
                            data-src="assets/images/form-important-star.svg" class="js-lazy"></label>
                <input type="password" class="form-control" id="inputPassword4">
            </div>
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">استان <img data-src="assets/images/form-important-star.svg"
                                                                       class="js-lazy"></label>
                <div class="custom-select-container">
                    <div class="custom-select">
                        <select>
                            <option value="1">تهران</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label for="inputPassword4" class="form-label">شهر <img data-src="assets/images/form-important-star.svg"
                                                                        class="js-lazy"></label>
                <div class="custom-select-container">
                    <div class="custom-select">
                        <select>
                            <option value="1">تهران</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <label for="inputAddress" class="form-label">آدرس دقیق</label>
                <input type="text" class="form-control" id="inputAddress">
            </div>
            <div class="col-md-6">
                <label for="inputCity" class="form-label">کد پستی <img data-src="assets/images/form-important-star.svg"
                                                                       class="js-lazy"></label>
                <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="col-md-6">
                <label for="inputState" class="form-label">تلفن ثابت <span></span></label>
                <div class="custom-select-container">
                    <input type="text" class="form-control" id="inputState">
                </div>
            </div>
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">تلفن همراه <img data-src="assets/images/form-important-star.svg"
                                                                            class="js-lazy"></label>
                <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="col-md-6">
                <label for="inputPassword4" class="form-label">ایمیل <img data-src="assets/images/form-important-star.svg"
                                                                          class="js-lazy"></label>
                <input type="password" class="form-control" id="inputPassword4">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">توضیحات تکمیلی</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
        </form>
    </section>
    <aside class="shopping-cart-and-checkout-aside your-order">
        <div class="aside__heading">سفارش شما</div>
        <div class="aside__content-container">
            <table>
                <tr>
                    <td>محصول</td>
                    <td class="shopping-cart__price">قیمت</td>
                </tr>
                <tr>
                    <td>قاب موبایل</td>
                    <td class="shopping-cart__price">۷۱,۸۰۰ <span class="shopping-cart__toman">تومان</span></td>
                </tr>
                <tr>
                    <td>قیمت کل</td>
                    <td class="shopping-cart__price">15.000 <span class="shopping-cart__toman">تومان</span></td>
                </tr>
                <tr>
                    <td>حمل و نقل</td>
                    <td class="shopping-cart__price">15.000 <span class="shopping-cart__toman">تومان</span></td>
                </tr>
                <tr>
                    <td>مبلغ کل</td>
                    <td class="shopping-cart__price">15.000 <span class="shopping-cart__toman">تومان</span></td>
                </tr>
            </table>
            <div class="d-flex align-items-center justify-content-between checkout-aside__buttons-container flex-wrap text-nowrap">
                <label class="d-flex align-items-center">
                    <input type="radio" name="payment">
                    <div class="input-type-radio-outward"></div>
                    <img data-src="assets/images/parsian.png" class="mx-2 js-lazy">
                    <span>درگاه بانک پاسیان</span>
                </label>
                <label class="d-flex align-items-center">
                    <input type="radio" name="payment">
                    <div class="input-type-radio-outward"></div>
                    <img data-src="assets/images/zarinpal.png" class="mx-2 js-lazy">
                    <span>درگاه زرین‌پال</span>
                </label>
            </div>
            <a class="btn" href="">ثبت سفارش</a>
        </div>
    </aside>
</div>
<?php include 'footer.php'; ?>
</body>
</html>