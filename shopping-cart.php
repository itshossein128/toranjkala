<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-grey">
<?php include 'header.php'; ?>
<div class="breadcrumb-container">
    <nav class="container" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">صفحه اصلی</a></li>
            <li class="breadcrumb-item"><a href="#">فروشگاه</a></li>
            <li class="breadcrumb-item"><a href="#">لوازم منزل و فانتزی</a></li>
            <li class="breadcrumb-item"><a href="#">لوازم شخصی</a></li>
            <li class="breadcrumb-item active" aria-current="page">هندزفری پلاتینا مدل</li>
        </ol>
    </nav>
</div>
<div class="d-flex justify-content-between align-items-start flex-column flex-xl-row container">
    <section class="shopping-cart-main-section mb-5 mb-xl-0">
        <div class="table-responsive">
            <table class="table table-borderless m-0">
                <thead>
                <tr>
                    <th>محصول</th>
                    <th>قیمت</th>
                    <th>تعداد</th>
                    <th>جمع</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <img data-src="assets/images/product1.png" class="me-2 js-lazy">
                        هدفون بی‌ سیم هایلو مدل GT1
                    </td>
                    <td>۷۱٫۸۰۰ تومان</td>
                    <td>
                        <div class="shopping-cart__product-counter input-number-w-controllers">
                            <button class="input-inc"><img src="assets/images/plus.svg"></button>
                            <input type="number" value="1">
                            <button class="input-dec"><img src="assets/images/minus.svg"></button>
                        </div>
                    </td>
                    <td>۷۱٫۸۰۰ تومان</td>
                    <td>
                        <button class="border-0 bg-transparent remove-item-from-list-btn"><img data-src="assets/images/shopping-cart-remove-button.svg"
                                                                     class="js-lazy"></button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img data-src="assets/images/product1.png" class="me-2 js-lazy">
                        هدفون بی‌ سیم هایلو مدل GT1
                    </td>
                    <td>۷۱٫۸۰۰ تومان</td>
                    <td>
                        <div class="shopping-cart__product-counter input-number-w-controllers">
                            <button class="input-inc"><img src="assets/images/plus.svg"></button>
                            <input type="number" value="1">
                            <button class="input-dec"><img src="assets/images/minus.svg"></button>
                        </div>
                    </td>
                    <td>۷۱٫۸۰۰ تومان</td>
                    <td>
                        <button class="border-0 bg-transparent remove-item-from-list-btn"><img data-src="assets/images/shopping-cart-remove-button.svg"
                                                                     class="js-lazy"></button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="shopping-cart-main-section__footer d-flex flex-column flex-md-row align-items-start align-md-items-center justify-content-between">
            <div class="off-code-container mb-3 mb-md-0">
                <input type="text" placeholder="کد تخفیف">
                <button>اعمال کوپن</button>
            </div>
            <div>
                <a href="" class="continue-shopping">ادامه خرید</a>
                <button class="update-shopping-cart-btn"> بروزرسانی سبد خرید</button>
            </div>
        </div>
    </section>
    <aside class="shopping-cart-and-checkout-aside shopping-cart-aside">
        <div class="aside__heading">فاکتور سبد خرید</div>
        <div class="aside__content-container">
            <table>
                <tr>
                    <td>مجموع مصولات</td>
                    <td class="shopping-cart__price">۷۱,۸۰۰ <span class="shopping-cart__toman">تومان</span></td>
                </tr>
                <tr>
                    <td>هزینه ارسال</td>
                    <td class="shopping-cart__price">15.000 <span class="shopping-cart__toman">تومان</span></td>
                </tr>
                <tr>
                    <td>مبلغ کل</td>
                    <td class="shopping-cart__price">15.000 <span class="shopping-cart__toman">تومان</span></td>
                </tr>
            </table>
            <a class="btn" href="">اتمام خرید</a>
        </div>
    </aside>
</div>
<?php include 'footer.php'; ?>
</body>
</html>