<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-grey">
<?php include 'header.php'; ?>
<div class="breadcrumb-container">
    <nav class="container" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">صفحه اصلی</a></li>
            <li class="breadcrumb-item"><a href="#">فروشگاه</a></li>
            <li class="breadcrumb-item"><a href="#">لوازم منزل و فانتزی</a></li>
            <li class="breadcrumb-item"><a href="#">لوازم شخصی</a></li>
            <li class="breadcrumb-item active" aria-current="page">هندزفری پلاتینا مدل</li>
        </ol>
    </nav>
</div>
<div class="d-flex justify-content-between align-items-start flex-column flex-md-row container">
    <div class="login align-self-stretch d-flex flex-column">
        <span class="login-page__heading">ورود</span>
        <form class="h-100 d-flex flex-column justify-content-between">
            <div>
                <div class="">
                    <label for="inputEmail4" class="form-label"><img data-src="assets/images/form-important-star.svg" class="js-lazy">
                        نام
                        کاربری یا ایمیل </label>
                    <input type="email" class="form-control" id="inputEmail4">
                </div>
                <div class="">
                    <label for="inputEmail4" class="form-label"><img data-src="assets/images/form-important-star.svg" class="js-lazy">
                        گذرواژه </label>
                    <input type="email" class="form-control" id="inputEmail4">
                </div>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked">
                <label class="form-check-label" for="flexCheckChecked">
                    به خاطرسپاری
                </label>
            </div>
            <div class="d-flex align-items-xl-center align-items-start flex-column flex-xl-row ">
                <button class="login-btn me-5">ورود</button>
                <a class="text-nowrap mt-4 mt-xl-0">گذرواژه خود را فراموش کرده‌اید؟</a>
            </div>
        </form>
    </div>
    <div class="signup align-self-stretch d-flex flex-column">
        <span class="login-page__heading">ثبت نام</span>
        <form class="h-100 d-flex flex-column justify-content-between">
            <div>
                <div class="">
                    <label for="inputEmail4" class="form-label"><img data-src="assets/images/form-important-star.svg" class="js-lazy">
                        نام
                        کاربری یا ایمیل </label>
                    <input type="email" class="form-control" id="inputEmail4">
                </div>
                <div class="">
                    <label for="inputEmail4" class="form-label"><img data-src="assets/images/form-important-star.svg" class="js-lazy">
                        گذرواژه </label>
                    <input type="email" class="form-control" id="inputEmail4">
                </div>
            </div>
            <button class="signup-btn">ثبت نام</button>
        </form>
    </div>
</div>
<?php include 'footer.php'; ?>
</body>
</html>