<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-grey">
<?php include 'header.php'; ?>
<section class="dashboard-section container d-flex align-items-start justify-content-between">
    <aside class="dashboard-aside d-none d-lg-block">
        <div class="profile-container">
            <img src="assets/images/no-pic-profile.svg" alt="">
            <div class="d-flex flex-column align-items-start ms-2">
                <span>هومن حسنی</span>
                <span>۰۹۳۵۳۵۵۷۴۱۴</span>
            </div>
        </div>
        <ul>
            <li>
                <a href="#">
                    <img src="assets/images/dashboard.svg" alt="">
                    <img src="assets/images/dashboard-active.svg" class="dashboard-icon-active">
                    داشبورد
                </a>
            </li>
            <li class="dashboard-active">
                <a href="#">
                    <img src="assets/images/orders.svg" alt="">
                    <img src="assets/images/orders-active.svg" class="dashboard-icon-active">
                    سفارش‌های من
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="assets/images/dashboard-location.svg" alt="">
                    <img src="assets/images/dashboard-location-active.svg" class="dashboard-icon-active">
                    نشانی‌ها
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="assets/images/account.svg" alt="">
                    <img src="assets/images/account-active.svg" class="dashboard-icon-active">
                    حساب کاربری
                </a>
            </li>
            <li class="dashboard-li-separator"></li>
            <li>
                <a href="#">
                    <img src="assets/images/exit.svg" alt="">
                    <img src="assets/images/exit-active.svg" class="dashboard-icon-active">
                    خروج
                </a>
            </li>
        </ul>
    </aside>
    <div class="last-transaction">
        <span class="dashboard__heading">آخرین تراکنش‌ها</span>
        <div class="last-transaction__table-container">
            <div class="table-responsive-lg">
                <table class="table table-borderless text-center">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>شناسه پرداخت</th>
                        <th>تاریخ</th>
                        <th>محصول</th>
                        <th>مبلغ کل</th>
                        <th>عملیات پرداخت</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>48459453</td>
                        <td>۲۳ خرداد ۱۳۹۹</td>
                        <td>قاب موبایل</td>
                        <td>650.000 تومان</td>
                        <td><span class="success-payment">پرداخت موفق</span></td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>48459453</td>
                        <td>۲۳ خرداد ۱۳۹۹</td>
                        <td>قاب موبایل</td>
                        <td>650.000 تومان</td>
                        <td><span class="success-payment">پرداخت موفق</span></td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>48459453</td>
                        <td>۲۳ خرداد ۱۳۹۹</td>
                        <td>قاب موبایل</td>
                        <td>650.000 تومان</td>
                        <td><span class="success-payment">پرداخت موفق</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <a class="see-all-transactions btn">مشاهده همه تراکنش ها</a>
        </div>
    </div>
</section>
<?php include 'footer.php'; ?>
</body>
</html>