<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-grey">
<?php include 'header.php'; ?>
<section class="overflow-hidden mt-5">
    <div class="container-fluid">
        <!-- Swiper -->
        <div class="swiper index-slider dir-ltr overflow-visible">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <a href="">
                        <img src="assets/images/slide1.png" alt="">
                        <div class="position-absolute">
                            <span class="index-slider__off">تا ۳۰٪ تخفیف</span>
                            <span class="index-slider__title">قاب موبایل</span>
                            <button class="index-slider__button"><img src="assets/images/slider-button-icon.svg"
                                                                      class="ms-2"> همین حالا خرید کنید
                            </button>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="">
                        <img src="assets/images/slide2.png" alt="">
                        <div class="position-absolute">
                            <span class="index-slider__off">تا ۳۰٪ تخفیف</span>
                            <span class="index-slider__title">قاب موبایل</span>
                            <button class="index-slider__button"><img src="assets/images/slider-button-icon.svg"
                                                                      class="ms-2"> همین حالا خرید کنید
                            </button>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="">
                        <img src="assets/images/slide3.png" alt="">
                        <div class="position-absolute">
                            <span class="index-slider__off">تا ۳۰٪ تخفیف</span>
                            <span class="index-slider__title">قاب موبایل</span>
                            <button class="index-slider__button"><img src="assets/images/slider-button-icon.svg"
                                                                      class="ms-2"> همین حالا خرید کنید
                            </button>
                        </div>
                    </a>
                </div>
            </div>
            <div class="index-slider__pagination"></div>
        </div>
    </div>
</section>
<section class="container category-slider__container overflow-hidden">
    <div class="swiper category-slider dir-ltr">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <img src="assets/images/product3.png" class="mb-1">
                <span>شارژد</span>
            </div>
            <div class="swiper-slide">
                <img src="assets/images/product3.png" class="mb-1">
                <span>لپتاپ</span>
            </div>
            <div class="swiper-slide">
                <img src="assets/images/product3.png" class="mb-1">
                <span>لنز</span>
            </div>
            <div class="swiper-slide">
                <img src="assets/images/product3.png" class="mb-1">
                <span>فلش</span>
            </div>
            <div class="swiper-slide">
                <img src="assets/images/product3.png" class="mb-1">
                <span>پرینتر</span>
            </div>
            <div class="swiper-slide">
                <img src="assets/images/product3.png" class="mb-1">
                <span>شارژد</span>
            </div>
            <div class="swiper-slide">
                <img src="assets/images/product3.png" class="mb-1">
                <span>هدفون</span>
            </div>
            <div class="swiper-slide">
                <img src="assets/images/product3.png" class="mb-1">
                <span>اسپیکر</span>
            </div>
        </div>
        <div class="category-slider__button-next">
            <img src="assets/images/category-next.svg" alt="">
        </div>
        <div class="category-slider__button-prev">
            <img src="assets/images/category-prev.svg" alt="">
        </div>
    </div>
</section>
<section class="container-sm instant-offer-section overflow-hidden">
    <div class="d-flex justify-content-between align-items-center instant-offer__heading-container">
        <span class="instant-offer__heading"><img src="assets/images/instant-offer.svg"
                                                  class="me-2"> پیشنهادات لحظه‌ای</span>
        <div class="d-flex justify-content-between align-items-center mobile-size-slider-buttons-container">
            <div class="instant-offer__button-next d-block d-sm-none"><img src="assets/images/angel-prev.svg"
                                                                           class="angel-next">
            </div>
            <div class="instant-offer__button-prev d-flex justify-content-end align-items-center align-self-stretch d-sm-none">
                <img src="assets/images/angel-prev.svg" alt="">
            </div>
        </div>
    </div>
    <div class="instant-offer-container dir-ltr">
        <div class="main-slider instant-offer swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide1img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide2img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide3img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide4img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide1img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide1img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="instant-offer__button-prev d-none d-sm-block"><img src="assets/images/category-prev.svg" alt="">
        </div>
        <div class="instant-offer__button-next d-none d-sm-block"><img src="assets/images/category-next.svg" alt="">
        </div>
        <div class="instant-offer-pagination dir-ltr d-flex justify-content-center"></div>
    </div>
</section>
<section class="offer-cards container mt-5">
    <div class="row">
        <div class="position-relative col-12 col-lg-4 mt-4 mt-lg-0">
            <img src="assets/images/cart-bg1.png" class="img-fluid w-100">
            <div class="position-absolute d-flex flex-column align-items-center justify-content-center">
                <span class="offer-cards__first-line">شروع قیمت از 150.000 تومان</span>
                <span class="offer-cards__second-line">زیور آلات</span>
                <button class="index-slider__button">
                    <img src="assets/images/slider-button-icon.svg"
                         class="ms-2"> همین حالا خرید کنید
                </button>
            </div>
        </div>
        <div class="position-relative col-12 col-lg-4 mt-4 mt-lg-0">
            <img src="assets/images/cart-bg1.png" class="img-fluid w-100">
            <div class="position-absolute d-flex flex-column align-items-center justify-content-center">
                <span class="offer-cards__first-line">شروع قیمت از 150.000 تومان</span>
                <span class="offer-cards__second-line">زیور آلات</span>
                <button class="index-slider__button">
                    <img src="assets/images/slider-button-icon.svg"
                         class="ms-2"> همین حالا خرید کنید
                </button>
            </div>
        </div>
        <div class="position-relative col-12 col-lg-4 mt-4 mt-lg-0">
            <img src="assets/images/cart-bg1.png" class="img-fluid w-100">
            <div class="position-absolute d-flex flex-column align-items-center justify-content-center">
                <span class="offer-cards__first-line">شروع قیمت از 150.000 تومان</span>
                <span class="offer-cards__second-line">زیور آلات</span>
                <button class="index-slider__button">
                    <img src="assets/images/slider-button-icon.svg"
                         class="ms-2"> همین حالا خرید کنید
                </button>
            </div>
        </div>
    </div>
</section>
<section class="special-offer-section container d-flex flex-column flex-lg-row align-items-center">
    <div class="special-offers__timer">
        <span>SPECIAL OFFER</span>
        <span class="font-50-heavy">پیشنهادهای</span>
        <span class="font-50-heavy text-my-orange">ویژه</span>
        <div class="d-flex align-items-center justify-content-center flex-column">
            <div class="d-flex align-items-center justify-content-between w-100">
                <div class="special-offer-time special-offer__minutes" data-time="54">
                    <div>۰</div>
                    <div>۰</div>
                </div>
                <span>:</span>
                <div class="special-offer-time special-offer__hours" data-time="16">
                    <div>۰</div>
                    <div>۰</div>
                </div>
                <span>:</span>
                <div class="special-offer-time special-offer__days" data-time="3">
                    <div>۰</div>
                    <div>۰</div>
                </div>
            </div>
            <div class="d-flex w-100 special-offer__values-container">
                <div>دقیقه</div>
                <div>ساعت</div>
                <div>روز</div>
            </div>
        </div>
    </div>
    <div class="swiper special-offer-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide my-cart off-active">
                <a href="#">
                    <div class="my-cart__off">۲۰٪</div>
                    <div class="my-cart__img-container">
                        <img src="assets/images/slide2img.png" class="img-fluid">
                    </div>
                    <div class="my-cart__footer">
                        <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                        <div class="my-cart__footer__price">
                            <span>۷۱,۸۰۰</span>
                            <span>تومن</span>
                            <del>۱۱۰.۰۰۰</del>
                        </div>
                    </div>
                </a>
            </div>
            <div class="swiper-slide my-cart off-active">
                <a href="#">
                    <div class="my-cart__off">۲۰٪</div>
                    <div class="my-cart__img-container">
                        <img src="assets/images/slide2img.png" class="img-fluid">
                    </div>
                    <div class="my-cart__footer">
                        <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                        <div class="my-cart__footer__price">
                            <span>۷۱,۸۰۰</span>
                            <span>تومن</span>
                            <del>۱۱۰.۰۰۰</del>
                        </div>
                    </div>
                </a>
            </div>
            <div class="swiper-slide my-cart off-active">
                <a href="#">
                    <div class="my-cart__off">۲۰٪</div>
                    <div class="my-cart__img-container">
                        <img src="assets/images/slide2img.png" class="img-fluid">
                    </div>
                    <div class="my-cart__footer">
                        <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                        <div class="my-cart__footer__price">
                            <span>۷۱,۸۰۰</span>
                            <span>تومن</span>
                            <del>۱۱۰.۰۰۰</del>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="special-offer__pagination"></div>
    </div>
</section>
<section class="container-sm instant-offer-section special-package">
    <div class="d-flex justify-content-between align-items-center instant-offer__heading-container">
        <span class="instant-offer__heading">پکیج ویژه</span>
        <div class="d-flex justify-content-between align-items-center mobile-size-slider-buttons-container">
            <div class="instant-offer__button-next d-block"><img src="assets/images/angel-prev.svg"
                                                                 class="angel-next">
            </div>
            <div class="instant-offer__button-prev d-flex justify-content-end align-items-center align-self-stretch">
                <img src="assets/images/angel-prev.svg" alt="">
            </div>
        </div>
    </div>
    <div class="instant-offer-container special-package-container dir-ltr">
        <div class="main-slider special-package swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide1img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide2img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide3img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide4img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide1img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide1img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container-sm instant-offer-section best-selling-products">
    <div class="d-flex justify-content-between align-items-center instant-offer__heading-container">
        <span class="instant-offer__heading">پکیج ویژه</span>
        <div class="d-flex justify-content-between align-items-center mobile-size-slider-buttons-container">
            <div class="instant-offer__button-next d-block"><img src="assets/images/angel-prev.svg"
                                                                 class="angel-next">
            </div>
            <div class="instant-offer__button-prev d-flex justify-content-end align-items-center align-self-stretch">
                <img src="assets/images/angel-prev.svg" alt="">
            </div>
        </div>
    </div>
    <div class="instant-offer-container best-selling-products-container dir-ltr">
        <div class="main-slider special-package swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide1img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide2img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide3img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide4img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide1img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="swiper-slide my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img src="assets/images/slide1img.png" class="img-fluid">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container banner-container position-relative">
    <img src="assets/images/banner-bg.png" class="img-fluid">
    <img src="assets/images/iphone_13_PNG33.png" class="banner-right-side-img">
    <img src="assets/images/iPhone-13-PNG-Photos.png" class="banner-left-side-img">
    <div class="banner-content">
        <div class="banner-titre">
            <span>جدیدترین</span>
            <span>تلفن‌های هوشمند</span>
            <button>
                همین حالا خرید کنید
                <img src="assets/images/slider-button-icon.svg" alt="">
            </button>
        </div>
    </div>
</div>
<section class="container">
    <span class="instant-offer__heading"><img src="assets/images/instant-offer.svg"
                                              class="me-2"> پیشنهادات لحظه‌ای</span>
    <div class="instant-offer-cards-container">
        <div class="my-cart">
            <a href="#">
                <div class="my-cart__off">۲۰٪</div>
                <div class="my-cart__img-container">
                    <img src="assets/images/slide2img.png" class="img-fluid">
                </div>
                <div class="my-cart__footer">
                    <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                    <div class="my-cart__footer__price">
                        <span>۷۱,۸۰۰</span>
                        <span>تومن</span>
                        <del>۱۱۰.۰۰۰</del>
                    </div>
                </div>
            </a>
        </div>
        <div class="my-cart off-active">
            <a href="#">
                <div class="my-cart__off">۲۰٪</div>
                <div class="my-cart__img-container">
                    <img src="assets/images/slide1img.png" class="img-fluid">
                </div>
                <div class="my-cart__footer">
                    <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                    <div class="my-cart__footer__price">
                        <span>۷۱,۸۰۰</span>
                        <span>تومن</span>
                        <del>۱۱۰.۰۰۰</del>
                    </div>
                </div>
            </a>
        </div>
        <div class="my-cart off-active">
            <a href="#">
                <div class="my-cart__off">۲۰٪</div>
                <div class="my-cart__img-container">
                    <img src="assets/images/slide3img.png" class="img-fluid">
                </div>
                <div class="my-cart__footer">
                    <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                    <div class="my-cart__footer__price">
                        <span>۷۱,۸۰۰</span>
                        <span>تومن</span>
                        <del>۱۱۰.۰۰۰</del>
                    </div>
                </div>
            </a>
        </div>
        <div class="my-cart">
            <a href="#">
                <div class="my-cart__off">۲۰٪</div>
                <div class="my-cart__img-container">
                    <img src="assets/images/slide2img.png" class="img-fluid">
                </div>
                <div class="my-cart__footer">
                    <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                    <div class="my-cart__footer__price">
                        <span>۷۱,۸۰۰</span>
                        <span>تومن</span>
                        <del>۱۱۰.۰۰۰</del>
                    </div>
                </div>
            </a>
        </div>
        <div class="my-cart">
            <a href="#">
                <div class="my-cart__off">۲۰٪</div>
                <div class="my-cart__img-container">
                    <img src="assets/images/slide2img.png" class="img-fluid">
                </div>
                <div class="my-cart__footer">
                    <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                    <div class="my-cart__footer__price">
                        <span>۷۱,۸۰۰</span>
                        <span>تومن</span>
                        <del>۱۱۰.۰۰۰</del>
                    </div>
                </div>
            </a>
        </div>
        <div class="my-cart off-active">
            <a href="#">
                <div class="my-cart__off">۲۰٪</div>
                <div class="my-cart__img-container">
                    <img src="assets/images/slide1img.png" class="img-fluid">
                </div>
                <div class="my-cart__footer">
                    <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                    <div class="my-cart__footer__price">
                        <span>۷۱,۸۰۰</span>
                        <span>تومن</span>
                        <del>۱۱۰.۰۰۰</del>
                    </div>
                </div>
            </a>
        </div>
        <div class="my-cart off-active">
            <a href="#">
                <div class="my-cart__off">۲۰٪</div>
                <div class="my-cart__img-container">
                    <img src="assets/images/slide3img.png" class="img-fluid">
                </div>
                <div class="my-cart__footer">
                    <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                    <div class="my-cart__footer__price">
                        <span>۷۱,۸۰۰</span>
                        <span>تومن</span>
                        <del>۱۱۰.۰۰۰</del>
                    </div>
                </div>
            </a>
        </div>
        <div class="my-cart">
            <a href="#">
                <div class="my-cart__off">۲۰٪</div>
                <div class="my-cart__img-container">
                    <img src="assets/images/slide2img.png" class="img-fluid">
                </div>
                <div class="my-cart__footer">
                    <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                    <div class="my-cart__footer__price">
                        <span>۷۱,۸۰۰</span>
                        <span>تومن</span>
                        <del>۱۱۰.۰۰۰</del>
                    </div>
                </div>
            </a>
        </div>
    </div>
</section>
<section class="container">
    <div class="toranjkala-brands-container">
        <div class="d-flex justify-content-between align-items-center">
            <span><img src="assets/images/toranjkala-brands.svg" alt="">برندهای ترنج‌کالا</span>
            <div class="d-flex justify-content-between align-items-center mobile-size-slider-buttons-container">
                <div class="instant-offer__button-next d-block"><img src="assets/images/angel-prev.svg"
                                                                     class="angel-next">
                </div>
                <div class="instant-offer__button-prev d-flex justify-content-end align-items-center align-self-stretch">
                    <img src="assets/images/angel-prev.svg" alt="">
                </div>
            </div>
        </div>
        <div class="swiper toranjkala-brands">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="assets/images/brand1.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand2.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand3.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand1.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand2.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand3.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand1.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand2.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand3.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand1.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand2.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand3.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand1.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand2.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand3.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand1.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand2.png" class="img-fluid w-100">
                </div>
                <div class="swiper-slide">
                    <img src="assets/images/brand3.png" class="img-fluid w-100">
                </div>
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php'; ?>
</body>
</html>