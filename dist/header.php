<div class="header-container">
    <div class="container">
        <nav class="navbar navbar-expand-lg py-0">
            <div class="d-flex flex-column w-100">
                <div class="header-first-part d-grid align-items-center">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_nav"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <img src="assets/images/navbar-toggler-icon.svg" alt="">
                    </button>
                    <a class="navbar-brand m-0" href="#"><img src="assets/images/logo.svg" alt=""></a>
                    <div class="header-search-container d-none d-lg-flex justify-content-center">
                        <form class="">
                            <input class="h-100" type="search" placeholder="جستجوی محصولات...">
                            <button class="h-100"><img src="assets/images/search.svg" alt=""></button>
                            <ul class="d-none">
                                <li><a href="#">قاب موبایل</a></li>
                                <li><a href="#">قاب تبلت</a></li>
                            </ul>
                        </form>
                    </div>
                    <div class="collapse navbar-collapse d-inline-flex justify-content-end" id="main_nav">
                        <ul class="navbar-nav">
                            <li class="nav-item text-nowrap ms-3 d-none d-lg-block"><a class="nav-link" href="#"><img
                                            src="assets/images/check.svg" alt=""></a></li>
                            <li class="nav-item text-nowrap ms-3 d-none d-lg-block"><a class="nav-link" href="#"><img
                                            src="assets/images/heart.svg" alt=""></a></li>
                            <li class="nav-item text-nowrap ms-0 ms-lg-3 dropdown shopping-cart">
                                <a class="nav-link dropdown-toggle position-relative" href="#"
                                   data-bs-toggle="dropdown">
                                    <img src="assets/images/shopping-cart.svg" alt="">
                                    <span class="shopping-cart-badge">2</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <ul class="p-0">
                                        <li class="d-flex align-items-start justify-content-between mx-3 py-3">
                                            <a class="dropdown-item d-flex align-items-start p-0" href="#">
                                                <img src="assets/images/product1.png" alt="">
                                                <div class="d-flex flex-column align-items-start">
                                                    <span class="shopping-cart__title">هدفون بی‌ سیم هایلو مدل GT1</span>
                                                    <span class="shopping-cart__price">۷۱,۸۰۰ تومان</span>
                                                </div>
                                            </a>
                                            <button class="shopping-cart__close-btn"><img src="assets/images/close.svg"
                                                                                          alt=""></button>
                                        </li>
                                        <li class="d-flex align-items-start justify-content-between mx-3 py-3">
                                            <a class="dropdown-item d-flex align-items- p-0" href="#">
                                                <img src="assets/images/product1.png" alt="">
                                                <div class="d-flex flex-column align-items-start">
                                                    <span class="shopping-cart__title">هدفون بی‌ سیم هایلو مدل GT1</span>
                                                    <span class="shopping-cart__price">۷۱,۸۰۰ تومان</span>
                                                </div>
                                            </a>
                                            <button class="shopping-cart__close-btn"><img src="assets/images/close.svg"
                                                                                          alt=""></button>
                                        </li>
                                    </ul>
                                    <div class="shopping-cart__links links mx-3 py-3 d-flex align-items-center justify-content-between">
                                        <a class="btn" href="#">
                                            <img src="assets/images/shopping-cart.svg" alt="">
                                            مشاهده سبد خرید
                                        </a>
                                        <a class="btn py-2 px-5" href="#">پرداخت</a>
                                    </div>
                                    <div class="shopping-cart__total-price mx-3 py-3 d-flex align-items-center justify-content-between">
                                        <span class="">مجموع</span>
                                        <span class="font-16">۷۱٫۸۰۰ <span class="font-12">تومان</span></span>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item text-nowrap nav__account-link ms-5 d-none d-lg-block">
                                <a class="nav-link" href="#">
                                    <img src="assets/images/account-icon.svg" class="me-1">
                                    حساب کاربری
                                </a>
                            </li>
                        </ul>
                    </div> <!-- navbar-collapse.// -->
                </div>
                <div class="d-none d-lg-flex align-items-center justify-content-between">
                    <ul class="navbar-nav">
                        <li class="nav-item me-3 dropdown navbar-nav__category-button">
                            <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">
                                <img src="assets/images/category-menu-icon.svg" alt="">
                                دسته‌بندی محصولات
                            </a>
                            <ul class="dropdown-menu w-100">
                                <li class="has-megasubmenu">
                                    <a class="dropdown-item" href="#"> موبایل </a>
                                    <div class="megasubmenu dropdown-menu">
                                        <div class="row m-0">
                                            <span class="megasubmenu__title">موبایل‌ها</span>
                                            <div class="col-6">
                                                <span class="title">برند</span>
                                                <ul class="list-unstyled">
                                                    <li><a class="d-block" href="#">اپل</a></li>
                                                    <li><a class="d-block" href="#">سامسونگ</a></li>
                                                    <li><a class="d-block" href="#">شیائومی</a></li>
                                                    <li><a class="d-block" href="#">هوآوی</a></li>
                                                </ul>
                                            </div><!-- end col-3 -->
                                            <div class="col-6">
                                                <span class="title">لب</span>
                                                <ul class="list-unstyled">
                                                    <li><a class="d-block" href="#">رژ لب</a></li>
                                                    <li><a class="d-block" href="#">بالم لب</a></li>
                                                    <li><a class="d-block" href="#">نرم و براق کننده لب</a></li>
                                                </ul>
                                            </div><!-- end col-3 -->
                                        </div><!-- end row -->
                                    </div>
                                </li>
                                <li><a class="dropdown-item" href="#">تبلت</a></li>
                                <li><a class="dropdown-item" href="#">قاب</a></li>
                                <li><a class="dropdown-item" href="#">هولدر</a></li>
                                <li class="has-megasubmenu">
                                    <a class="dropdown-item" href="#"> شارژر </a>
                                    <div class="megasubmenu dropdown-menu">
                                        <div class="row m-0">
                                            <span class="megasubmenu__title">موبایل‌ها</span>
                                            <div class="col-6">
                                                <span class="title">برند</span>
                                                <ul class="list-unstyled">
                                                    <li><a class="d-block" href="#">اپل</a></li>
                                                    <li><a class="d-block" href="#">سامسونگ</a></li>
                                                    <li><a class="d-block" href="#">شیائومی</a></li>
                                                    <li><a class="d-block" href="#">هوآوی</a></li>
                                                </ul>
                                            </div><!-- end col-3 -->
                                            <div class="col-6">
                                                <span class="title">لب</span>
                                                <ul class="list-unstyled">
                                                    <li><a class="d-block" href="#">رژ لب</a></li>
                                                    <li><a class="d-block" href="#">بالم لب</a></li>
                                                    <li><a class="d-block" href="#">نرم و براق کننده لب</a></li>
                                                </ul>
                                            </div><!-- end col-3 -->
                                        </div><!-- end row -->
                                    </div>
                                </li>
                                <li><a class="dropdown-item" href="#"> اسپیکر </a></li>
                                <li><a class="dropdown-item" href="#"> گلس </a></li>
                                <li><a class="dropdown-item" href="#"> فلش </a></li>
                            </ul>
                        </li>
                        <li class="nav-item me-3"><a class="nav-link" href="#"> مجله ترنج کالا </a></li>
                        <li class="nav-item me-3"><a class="nav-link" href="#"> برندها </a></li>
                        <li class="nav-item me-3"><a class="nav-link" href="#"> همکاری در فروش </a></li>
                    </ul>
                    <div class="dir-ltr nav-bar__contact-ways">
                        <a href="">
                            <img src="assets/images/sms.svg" class="ms-1">
                            toranjkala@gmail.com
                        </a>

                        <a href="">
                            <img src="assets/images/phone.svg" class="ms-1">
                            + 98(21) 1234 - 5678
                        </a>
                    </div>
                </div>
            </div> <!-- container-fluid.// -->
        </nav>
        <!-- ============= COMPONENT END// ============== -->
    </div>
</div>
<div class="shop-basket-mobile-size shopping-cart" style="display: none;">
    <div class="shop-basket-mobile-size-close">
        بستن
    </div>
    <ul class="p-0">
        <li class="d-flex align-items-start justify-content-between mx-3 py-3">
            <a class="dropdown-item d-flex align-items-start p-0" href="#">
                <img src="assets/images/product1.png" alt="">
                <div class="d-flex flex-column align-items-start">
                    <span class="shopping-cart__title">هدفون بی‌ سیم هایلو مدل GT1</span>
                    <span class="shopping-cart__price">۷۱,۸۰۰ تومان</span>
                </div>
            </a>
            <button class="shopping-cart__close-btn"><img src="assets/images/close.svg"
                                                          alt=""></button>
        </li>
        <li class="d-flex align-items-start justify-content-between mx-3 py-3">
            <a class="dropdown-item d-flex align-items- p-0" href="#">
                <img src="assets/images/product1.png" alt="">
                <div class="d-flex flex-column align-items-start">
                    <span class="shopping-cart__title">هدفون بی‌ سیم هایلو مدل GT1</span>
                    <span class="shopping-cart__price">۷۱,۸۰۰ تومان</span>
                </div>
            </a>
            <button class="shopping-cart__close-btn"><img src="assets/images/close.svg"
                                                          alt=""></button>
        </li>
    </ul>
    <div class="shopping-cart__links links mx-3 py-3 d-flex align-items-center justify-content-between">
        <a class="btn" href="#">
            <img src="assets/images/shopping-cart.svg" alt="">
            مشاهده سبد خرید
        </a>
        <a class="btn py-2 px-5" href="#">پرداخت</a>
    </div>
    <div class="shopping-cart__total-price mx-3 py-3 d-flex align-items-center justify-content-between">
        <span class="">مجموع</span>
        <span class="font-16">۷۱٫۸۰۰ <span class="font-12">تومان</span></span>
    </div>
</div>

<div class="mobile-menu" style="display: none;">
    <div class="mobile-menu-close">بستن</div>
    <div class="navbar-nav d-flex align-items-center justify-content-between flex-row px-2 py-4">
        <div class="d-flex align-items-center">
            <div class="nav-item text-nowrap ms-3 d-block d-lg-none"><a class="nav-link" href="#"><img src="assets/images/check.svg" alt=""></a></div>
            <div class="nav-item text-nowrap ms-3 d-block d-lg-none"><a class="nav-link" href="#"><img src="assets/images/heart.svg" alt=""></a></div>
        </div>
        <div class="nav-item text-nowrap nav__account-link ms-5 d-block d-lg-none">
            <a class="nav-link" href="#">
                <img src="assets/images/account-icon.svg" class="me-1">
                حساب کاربری
            </a>
        </div>
    </div>
    <div class="shop-filter-categories-container">
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 134px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
    </div>
    <div class="dir-ltr nav-bar__contact-ways justify-content-center">
        <a href="">
            <img src="assets/images/sms.svg" class="ms-1">
            toranjkala@gmail.com
        </a>

        <a href="">
            <img src="assets/images/phone.svg" class="ms-1">
            + 98(21) 1234 - 5678
        </a>
    </div>
</div>