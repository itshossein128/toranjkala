<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-grey">
<?php include 'header.php'; ?>
<section class="dashboard-section container d-flex align-items-start justify-content-between">
    <aside class="dashboard-aside d-none d-lg-block">
        <div class="profile-container">
            <img src="assets/images/no-pic-profile.svg" alt="">
            <div class="d-flex flex-column align-items-start ms-2">
                <span>هومن حسنی</span>
                <span>۰۹۳۵۳۵۵۷۴۱۴</span>
            </div>
        </div>
        <ul>
            <li>
                <a href="#">
                    <img src="assets/images/dashboard.svg" alt="">
                    <img src="assets/images/dashboard-active.svg" class="dashboard-icon-active">
                    داشبورد
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="assets/images/orders.svg" alt="">
                    <img src="assets/images/orders-active.svg" class="dashboard-icon-active">
                    سفارش‌های من
                </a>
            </li>
            <li class="dashboard-active">
                <a href="#">
                    <img src="assets/images/dashboard-location.svg" alt="">
                    <img src="assets/images/dashboard-location-active.svg" class="dashboard-icon-active">
                    نشانی‌ها
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="assets/images/account.svg" alt="">
                    <img src="assets/images/account-active.svg" class="dashboard-icon-active">
                    حساب کاربری
                </a>
            </li>
            <li class="dashboard-li-separator"></li>
            <li>
                <a href="#">
                    <img src="assets/images/exit.svg" alt="">
                    <img src="assets/images/exit-active.svg" class="dashboard-icon-active">
                    خروج
                </a>
            </li>
        </ul>
    </aside>
    <div class="last-transaction">
        <span class="dashboard__heading">نشانی‌ها</span>
        <div class="addresses-container">
            <div class="d-flex align-items-center justify-content-between">
                <span class="dashboard__address">خیابان آزادی، نرسیده به میدان انقلاب، کوچه کارگر، پلاک 50</span>
                <div class="d-flex align-items-center">
                    <button class="btn"><img src="assets/images/edit.svg" alt=""></button>
                    <button class="address-erase btn"><img src="assets/images/erase.svg"></button>
                </div>
            </div>
            <span>تهران، تهران</span>
            <span>۳۱۵۶۹۳۴۴۸۴</span>
            <span>۰۹۳۵۳۵۵۷۴۱۴</span>
            <button class="btn"><img src="assets/images/plus-in-circle.svg" class="me-2"> افزودن آدرس</button>
        </div>
    </div>
</section>
<?php include 'footer.php'; ?>
</body>
</html>