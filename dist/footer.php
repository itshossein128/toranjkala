<footer>
    <div class="d-flex align-items-center justify-content-between flex-column flex-md-row container">
        <div class="d-flex align-items-center justify-content-start">
            <img src="assets/images/dollar.svg" class="me-3">
            <div>
                <span>قیمت‌های مناسب</span>
                <br>
                <span>ارزان‌تر از همه جا</span>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-start">
            <img src="assets/images/dollar.svg" class="me-3">
            <div>
                <span>قیمت‌های مناسب</span>
                <br>
                <span>ارزان‌تر از همه جا</span>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-start">
            <img src="assets/images/dollar.svg" class="me-3">
            <div>
                <span>قیمت‌های مناسب</span>
                <br>
                <span>ارزان‌تر از همه جا</span>
            </div>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="">
                <img src="assets/images/logo-white.svg" alt="">
                <br>
                <span class="d-block mt-3 font-12">فروشگاه آنلاین ترنج کالا با هدف فروش محصولات مختلف در دسته‌بندی‌ های تلفن های هوشمند، لوازم جانبی و ... فعالیت خود را آغاز نموده است.</span>
                <br>
                <div class="counseling-container">
                    <img src="assets/images/footer-bg.png" class="footer-bg img-fluid">
                    <div class="position-absolute d-flex align-items-center justify-content-start">
                        <img src="assets/images/24-hours.svg" class="me-2">
                        <div class="">
                            <span class="d-block">مشاوره پیش از خرید</span>
                            <a href="021-66128075">021-66128075</a>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <span class="footer__ul__heading">دسته‌بندی‌ها</span>
                <ul class="footer__ul-2-cols">
                    <li>لوازم جانبی موبایل</li>
                    <li>لوازم جانبی موبایل</li>
                    <li>لوازم جانبی موبایل</li>
                    <li>لوازم جانبی موبایل</li>
                    <li>لوازم جانبی موبایل</li>
                    <li>لوازم جانبی موبایل</li>
                    <li>لوازم جانبی موبایل</li>
                    <li>لوازم جانبی موبایل</li>
                </ul>
            </div>
            <div>
                <span class="footer__ul__heading">دیگر صفحات</span>
                <ul>
                    <li>خانه</li>
                    <li>خانه</li>
                    <li>خانه</li>
                    <li>همکاری در فروش</li>
                </ul>
            </div>
            <div>
                <span class="footer__ul__heading">خدمات مشتریان</span>
                <ul>
                    <li>خانه</li>
                    <li>خانه</li>
                    <li>خانه</li>
                    <li>قوانین و مقررات</li>
                </ul>
            </div>
            <div>
                <div class="nemad-container mb-3">
                    <img src="assets/images/nemad.png" alt="">
                </div>
                <div class="neshan-container">
                    <img src="assets/images/neshan.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="d-flex flex-column flex-sm-row align-items-center align-items-sm-start justify-content-between py-4">
                <div class="location">
                    <img src="assets/images/location.svg" class="me-2 d-none d-sm-block">
                    <span class="text-center text-sm-start">شیراز، کارگر شمالی خیابان نصرت نرسیده به جمالزاده کوچه بنده ای پلاک 15 واحد 1</span>
                </div>
                <div class="social-media d-flex align-items-center justify-content-center justify-content-sm-end">
                    <img src="assets/images/youtube.svg" class="ms-sm-3">
                    <img src="assets/images/pinterest.svg" class="ms-3">
                    <img src="assets/images/instagram.svg" class="ms-3">
                    <img src="assets/images/twitter.svg" class="ms-3">
                    <img src="assets/images/facebook.svg" class="ms-3">
                </div>
            </div>
            <div class="d-flex flex-column flex-md-row align-items-start justify-content-between">
                <span class="m-auto m-md-0">ساعات پاسخگویی: شنبه تا چهارشنبه ۹ الی ۱۸:۰۰ / پنجشنبه ۹ الی ۱۷:۰۰</span>
                <span class="m-auto m-md-0">کلیه حقوق برای ترنج کالا محفوظ است - 1401</span>
            </div>
        </div>
    </div>
</footer>
<script src="js/scripts.js"></script>
