<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-grey">
<?php include 'header.php'; ?>
<section class="dashboard-section container d-flex align-items-start justify-content-between">
    <aside class="dashboard-aside d-none d-lg-block">
        <div class="profile-container">
            <img src="assets/images/no-pic-profile.svg" alt="">
            <div class="d-flex flex-column align-items-start ms-2">
                <span>هومن حسنی</span>
                <span>۰۹۳۵۳۵۵۷۴۱۴</span>
            </div>
        </div>
        <ul>
            <li>
                <a href="#">
                    <img src="assets/images/dashboard.svg" alt="">
                    <img src="assets/images/dashboard-active.svg" class="dashboard-icon-active">
                    داشبورد
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="assets/images/orders.svg" alt="">
                    <img src="assets/images/orders-active.svg" class="dashboard-icon-active">
                    سفارش‌های من
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="assets/images/dashboard-location.svg" alt="">
                    <img src="assets/images/dashboard-location-active.svg" class="dashboard-icon-active">
                    نشانی‌ها
                </a>
            </li>
            <li class="dashboard-active">
                <a href="#">
                    <img src="assets/images/account.svg" alt="">
                    <img src="assets/images/account-active.svg" class="dashboard-icon-active">
                    حساب کاربری
                </a>
            </li>
            <li class="dashboard-li-separator"></li>
            <li>
                <a href="#">
                    <img src="assets/images/exit.svg" alt="">
                    <img src="assets/images/exit-active.svg" class="dashboard-icon-active">
                    خروج
                </a>
            </li>
        </ul>
    </aside>
    <div class="last-transaction">
        <span class="dashboard__heading">اطلاعات حساب</span>
        <div class="acc-info-container row">
            <div class="col-12 col-md-6">
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>نام و نام خانوادگی</span>
                        <span>هومن حسنی</span>
                    </div>
                    <div>
                        <button class="btn">
                            <img src="assets/images/edit.svg" alt="">
                        </button>
                    </div>
                </div>
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>تاریخ تولد</span>
                        <span>18 شهریور 1372</span>
                    </div>
                    <div>
                        <button class="btn">
                            <img src="assets/images/edit.svg" alt="">
                        </button>
                    </div>
                </div>
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>شماره همراه</span>
                        <span>09353557414</span>
                    </div>
                    <div>
                        <button class="btn">
                            <img src="assets/images/edit.svg" alt="">
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>کد ملی</span>
                        <span>001-1234568</span>
                    </div>
                    <div>
                        <button class="btn">
                            <img src="assets/images/edit.svg" alt="">
                        </button>
                    </div>
                </div>
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>ایمیل</span>
                        <span>hooman.hassani@gmail.com</span>
                    </div>
                    <div>
                        <button class="btn">
                            <img src="assets/images/edit.svg" alt="">
                        </button>
                    </div>
                </div>
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>رمز</span>
                        <span>********</span>
                    </div>
                    <div>
                        <button class="btn">
                            <img src="assets/images/edit.svg" alt="">
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php'; ?>
</body>
</html>