document.addEventListener("DOMContentLoaded", function () {
    /////// Prevent closing from click inside dropdown
    document.querySelectorAll('.dropdown-menu').forEach(function (element) {
        element.addEventListener('click', function (e) {
            e.stopPropagation();
        });
    });

    // make it as accordion for smaller screens
    if (window.innerWidth < 992) {
        document.querySelectorAll('.has-megasubmenu a').forEach(function (element) {
            element.addEventListener('mouseover', function (e) {

                let nextEl = this.nextElementSibling;
                if (nextEl && nextEl.classList.contains('megasubmenu')) {
                    // prevent opening link if link needs to open dropdown
                    e.preventDefault();
                    if (nextEl.style.display == 'block') {
                        nextEl.style.display = 'none';
                    } else {
                        nextEl.style.display = 'block';
                    }
                }
            });
        })
    }
    // end if innerWidth
});
// DOMContentLoaded  end

//shopping card remove button
if (document.querySelector('.shopping-cart')) {
    document.querySelectorAll('.shopping-cart__close-btn').forEach(function (item) {
        item.addEventListener('click', function () {
            //remove li tag
            this.parentElement.remove()
        })
    })
}
//shopping card remove button End


// index first slider
var indexSlider = new Swiper(".index-slider", {
    watchSlidesProgress: true,
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    centeredSlides: true,
    breakpoints: {
        576: {
            slidesPerView: 1.2,
        },
        768: {
            slidesPerView: 1.4,
        },
        1200: {
            slidesPerView: 1.6,
        },
        1400: {
            slidesPerView: 2,
        },
    },
    autoplay: {
        delay: 3000,
        pauseOnMouseEnter: true,
    },
    pagination: {
        el: ".index-slider__pagination",
    },
});

// index first slider
if (document.querySelector('.category-slider')) {
    var categorySlider = new Swiper(".category-slider", {
        watchSlidesProgress: true,
        slidesPerView: 2,
        breakpoints: {
            576: {
                slidesPerView: 'auto',
            },
        },
        navigation: {
            nextEl: '.category-slider__button-next',
            prevEl: '.category-slider__button-prev',
        },
    });

    window.addEventListener('resize', function () {
        if (document.querySelector('.container').clientWidth < (document.querySelector('.category-slider .swiper-slide').clientWidth * 8)) {
        }
    })
}
if (document.querySelector('.instant-offer')) {
    var mainSlider = new Swiper(".main-slider", {
        watchSlidesProgress: true,
        slidesPerView: 1.4,
        spaceBetween: 30,
        loop: true,
        centeredSlides: true,
        breakpoints: {
            576: {
                slidesPerView: 2,
                centeredSlides: false,
            },
            768: {
                slidesPerView: 4,
                centeredSlides: false,
            },
        },
        navigation: {
            nextEl: '.instant-offer__button-next',
            prevEl: '.instant-offer__button-prev',
        },
        pagination: {
            el: ".instant-offer-pagination",
            clickable: true,
        }
    });
}

if (document.querySelector('.special-package')) {
    var specialPackage = new Swiper(".special-package", {
        watchSlidesProgress: true,
        slidesPerView: 1.4,
        spaceBetween: 30,
        loop: true,
        centeredSlides: true,
        breakpoints: {
            576: {
                slidesPerView: 2,
                centeredSlides: false,
            },
            768: {
                slidesPerView: 4,
                centeredSlides: false,
            },
        },
        navigation: {
            nextEl: '.special-package__button-next',
            prevEl: '.special-package__button-prev',
        },
        pagination: {
            el: ".instant-offer-pagination",
            clickable: true,
        }
    });
}

if (document.querySelector('.best-selling-products')) {
    var specialPackage = new Swiper(".best-selling-products", {
        watchSlidesProgress: true,
        slidesPerView: 1.4,
        spaceBetween: 30,
        loop: true,
        centeredSlides: true,
        breakpoints: {
            576: {
                slidesPerView: 2,
                centeredSlides: false,
            },
            768: {
                slidesPerView: 4,
                centeredSlides: false,
            },
        },
        navigation: {
            nextEl: '.best-selling-products__button-next',
            prevEl: '.best-selling-products__button-prev',
        },
        pagination: {
            el: ".instant-offer-pagination",
            clickable: true,
        }
    });
}

if (document.querySelector('.special-offer-slider')) {
    var specialOfferSlider = new Swiper(".special-offer-slider", {
        watchSlidesProgress: true,
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,

        breakpoints: {
            992: {
                slidesPerView: 1.8,
            },
            1200: {
                slidesPerView: 2.8,
            },
        },

        pagination: {
            el: ".special-offer__pagination",
            clickable: true,
        }
    });
}


if (document.querySelector('.special-offer-time')) {
    let minutes = document.querySelector('.special-offer__minutes').dataset.time,
        hours = document.querySelector('.special-offer__hours').dataset.time,
        days = document.querySelector('.special-offer__days').dataset.time;

    function setTime() {
        let minutesText = minutes.toString()
        let hoursText = hours.toString()
        let daysText = days.toString()
        if (minutes < 10) {
            document.querySelector('.special-offer__minutes > div:first-child').innerHTML = minutesText[0]
            document.querySelector('.special-offer__minutes > div:last-child').innerHTML = '0';
        } else {
            document.querySelector('.special-offer__minutes > div:first-child').innerHTML = minutesText[1]
            document.querySelector('.special-offer__minutes > div:last-child').innerHTML = minutesText[0]
        }
        if (hours < 10) {
            document.querySelector('.special-offer__hours > div:first-child').innerHTML = hoursText[0]
            document.querySelector('.special-offer__hours > div:last-child').innerHTML = '0'
        } else {
            document.querySelector('.special-offer__hours > div:first-child').innerHTML = hoursText[1]
            document.querySelector('.special-offer__hours > div:last-child').innerHTML = hoursText[0]
        }
        if (days < 10) {
            document.querySelector('.special-offer__days > div:first-child').innerHTML = daysText[0]
            document.querySelector('.special-offer__days > div:last-child').innerHTML = '0'
        } else {
            document.querySelector('.special-offer__days > div:first-child').innerHTML = daysText[1]
            document.querySelector('.special-offer__days > div:last-child').innerHTML = daysText[0]
        }

    }

    setTime()
    let timerInterval = setInterval(function () {
        minutes--
        if (minutes < 0) {
            minutes = 59
            hours--
        }
        if (hours < 0) {
            hours = 23
            days--
            if (days < 0) {
                clearInterval(timerInterval)
                return
            }
        }
        setTime()
    }, 60000)
}


var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    ll = selElmnt.length;
    /*for each element, create a new DIV that will act as the selected item:*/
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected closed-select-border-radius");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /*for each element, create a new DIV that will contain the option list:*/
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < ll; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function (e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h, sl, yl;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            sl = s.length;
            h = this.parentNode.previousSibling;
            for (i = 0; i < sl; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    yl = y.length;
                    for (k = 0; k < yl; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function (e) {
        /*when the select box is clicked, close any other select boxes,
        and open/close the current select box:*/
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle('closed-select-border-radius')
        this.classList.toggle("select-arrow-active");
    });
}

function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, xl, yl, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    xl = x.length;
    yl = y.length;
    for (i = 0; i < yl; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < xl; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
            x[i].previousElementSibling.classList.add('closed-select-border-radius')
        }
    }
}

/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);


//product-single slider initialization
if (document.querySelector('.product-single-slider')) {
    var productSingleSlideThumb = new Swiper(".product-single-slider-thumb", {
        watchSlidesProgress: true,
        spaceBetween: 10,
        slidesPerView: 4,
    });

    var productSingleSlide = new Swiper(".product-single-slider", {
        watchSlidesProgress: true,
        thumbs: {
            swiper: productSingleSlideThumb,
        },
    });
}

//shop range slider
if (document.querySelector('#RangeSlider')) {

    const rangeSlider_min = 0;
    const rangeSlider_max = 100;
    const minToman = document.querySelector('#RangeSlider').dataset.minToman
    const maxToman = document.querySelector('#RangeSlider').dataset.maxToman
    const tomanRange = maxToman - minToman

    document.querySelector('#RangeSlider .range-slider-val-left').style.width = `${rangeSlider_min + (100 - rangeSlider_max)}%`;
    document.querySelector('#RangeSlider .range-slider-val-right').style.width = `${rangeSlider_min + (100 - rangeSlider_max)}%`;

    document.querySelector('#RangeSlider .range-slider-val-range').style.left = `${rangeSlider_min}%`;
    document.querySelector('#RangeSlider .range-slider-val-range').style.right = `${(100 - rangeSlider_max)}%`;

    document.querySelector('#RangeSlider .range-slider-handle-left').style.left = `${rangeSlider_min}%`;
    document.querySelector('#RangeSlider .range-slider-handle-right').style.left = `${rangeSlider_max}%`;

    document.querySelector('.range-slider-tooltip-left').style.left = `${rangeSlider_min}%`;
    document.querySelector('.range-slider-tooltip-right').style.left = `${rangeSlider_max}%`;

    document.querySelector('.range-slider-tooltip-left').innerText = minToman;
    document.querySelector('.range-slider-tooltip-right').innerText = maxToman;

    document.querySelector('.range-slider-input-left').value = rangeSlider_min;
    document.querySelector('.range-slider-input-left').addEventListener('input', e => {
        e.target.value = Math.min(e.target.value, e.target.parentNode.childNodes[5].value - 1);
        var value = (100 / (parseInt(e.target.max) - parseInt(e.target.min))) * parseInt(e.target.value) - (100 / (parseInt(e.target.max) - parseInt(e.target.min))) * parseInt(e.target.min);

        var children = e.target.parentNode.childNodes[1].childNodes;
        children[1].style.width = `${value}%`;
        children[5].style.left = `${value}%`;
        children[7].style.left = `${value}%`;

        document.querySelector('.range-slider-tooltip-left').textContent = tomanRange * e.target.value / 100 + minToman;
    });

    document.querySelector('.range-slider-input-right').value = rangeSlider_max;
    document.querySelector('.range-slider-input-right').addEventListener('input', e => {
        e.target.value = Math.max(e.target.value, e.target.parentNode.childNodes[3].value - (-1));
        var value = (100 / (parseInt(e.target.max) - parseInt(e.target.min))) * parseInt(e.target.value) - (100 / (parseInt(e.target.max) - parseInt(e.target.min))) * parseInt(e.target.min);

        var children = e.target.parentNode.childNodes[1].childNodes;
        children[3].style.width = `${100 - value}%`;
        children[5].style.right = `${100 - value}%`;
        children[9].style.left = `${value}%`;

        document.querySelector('.range-slider-tooltip-right').textContent = tomanRange * e.target.value / 100 + minToman;
    });
}


//set height of opened shop aside ul
if (document.querySelector('.shop-filter-categories-container')) {
    document.querySelectorAll('.shop-filter-categories-container ul').forEach(function (item) {
        setTimeout(function () {
            item.style.height = item.scrollHeight + 'px'
        }, 10)
    })

    document.querySelectorAll('.shop-filter-categories-container button').forEach(function (item) {
        item.addEventListener('click', function () {
            this.parentElement.classList.toggle('shop-filter__category-closed')
        })
    })
}

if (document.querySelector('.toranjkala-brands')) {
    var toranjkalaBrands = new Swiper(".toranjkala-brands", {
        watchSlidesProgress: true,
        slidesPerView: 2,
        spaceBetween: 10,

        navigation: {
            nextEl: '.toranjkala-brands__button-next',
            prevEl: '.toranjkala-brands__button-prev',
        },

        breakpoints: {
            480: {
                slidesPerView: 3,
            },
            576: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 35,
            },
            992: {
                slidesPerView: 6,
            },
        },
    });
}


document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('.mobile-menu').removeAttribute('style')
    document.querySelector('.shop-basket-mobile-size').removeAttribute('style')
})
//menu mobile
document.querySelector('.navbar-toggler').addEventListener('click', function () {
    document.querySelector('.mobile-menu').classList.add('show')
    document.querySelector('.mobile-menu-shadow').classList.add('show')
})
document.querySelector('.mobile-menu-close').addEventListener('click', function () {
    document.querySelector('.mobile-menu').classList.remove('show')
    document.querySelector('.mobile-menu-shadow').classList.remove('show')
})
document.querySelector('.mobile-menu-shadow').addEventListener('click', function () {
    document.querySelector('.mobile-menu').classList.remove('show')
    document.querySelector('.mobile-menu-shadow').classList.remove('show')
})
//shop basket
document.querySelector('.shopping-cart').addEventListener('click', function () {
    document.querySelector('.shop-basket-mobile-size').classList.add('show')
    document.querySelector('.mobile-shop-list-shadow').classList.add('show')
})
document.querySelector('.shop-basket-mobile-size-close').addEventListener('click', function () {
    document.querySelector('.shop-basket-mobile-size').classList.remove('show')
    document.querySelector('.mobile-shop-list-shadow').classList.remove('show')
})
document.querySelector('.mobile-shop-list-shadow').addEventListener('click', function () {
    document.querySelector('.shop-basket-mobile-size').classList.remove('show')
    document.querySelector('.mobile-shop-list-shadow').classList.remove('show')
})
if (document.querySelector('.product-info__headers-container')) {
    document.querySelectorAll('.product-info__headers-container button').forEach(function (item) {
        item.addEventListener('click', function () {
            document.querySelector('button.active').classList.remove('active')
            this.classList.add('active')
            document.querySelector('#video').classList.add('d-none')
            document.querySelector('#props-table').classList.add('d-none')
            document.querySelector('#comments').classList.add('d-none')
            document.querySelector(`#${this.dataset.target}`).classList.remove('d-none')
        })
    })
}


/* LazyLoad --> */
let lazyLoadInstance;
if (document.querySelector('.js-lazy')) {

    lazyLoadInstance = new LazyLoad({
        elements_selector: '.js-lazy',
        load_delay: 0,
        threshold: 200,
        unobserve_entered: true,
    });
}
/* LazyLoad <-- */

if (document.querySelector('.input-number-w-controllers')) {
    document.querySelectorAll('.input-number-w-controllers .input-inc').forEach(function (item) {
        item.addEventListener('click', function () {
            let currentValue = this.parentElement.querySelector('input').value
            this.parentElement.querySelector('input').value = +currentValue + 1
        })
    })
    document.querySelectorAll('.input-number-w-controllers .input-dec').forEach(function (item) {
        item.addEventListener('click', function () {
            let currentValue = this.parentElement.querySelector('input').value
            if (currentValue == 1) return
            this.parentElement.querySelector('input').value = +currentValue - 1
        })
    })
}

if (document.querySelector('.remove-item-from-list-btn')) {
    document.querySelectorAll('.remove-item-from-list-btn').forEach(function (item) {
        item.addEventListener('click', function () {
            this.parentElement.parentElement.remove()
        })
    })
}

if (document.querySelector('.header-search-container')) {
    let boldText = function (targetText) {
        document.querySelectorAll('.bold-text-observer').forEach(function (item) {
            let firstText = item.innerText;
            let boldedText = firstText.replace(targetText, `<span class="bold">${targetText}</span>`)
            item.innerHTML = boldedText
        })
    }

    function showSearchResult() {
        document.querySelector('.header-search-container ul').classList.remove('d-none')
        document.querySelector('.header-search-container button img:first-child').classList.add('d-none')
        document.querySelector('.header-search-container button img:last-child').classList.remove('d-none')

        //make searched text bold and orange
        boldText(this.value)
    }

    document.querySelector('.header-search-container input').addEventListener('focus', function (e) {
        document.querySelector('.header-search-container input').addEventListener('keyup', showSearchResult)
    })
    document.querySelector('.header-search-container').addEventListener('focusout', function () {
        document.querySelector('.header-search-container ul').classList.add('d-none')
        document.querySelector('.header-search-container button img:first-child').classList.remove('d-none')
        document.querySelector('.header-search-container button img:last-child').classList.add('d-none')

        document.querySelector('.header-search-container input').removeEventListener('keyup', showSearchResult)
        document.querySelector('.header-search-container input').value = ''
    })

    document.querySelector('.header-search-container button').addEventListener('click', function (e) {
        e.preventDefault()
        document.querySelector('.header-search-container button img:first-child').classList.remove('d-none')
        document.querySelector('.header-search-container button img:last-child').classList.add('d-none')
    })
}

if (document.querySelector('.addresses-container')) {
    if (document.querySelectorAll('.dashboard__address-container').length === 1) {
        document.querySelector('.address-erase').remove()
    }
    document.querySelectorAll('.address-erase').forEach(function (item) {
        item.addEventListener('click', function () {
            item.parentElement.parentElement.parentElement.remove()
            if (document.querySelectorAll('.dashboard__address-container').length === 1) {
                document.querySelector('.address-erase').remove()
            }
        })
    })
    document.querySelector('.add-address-form form').style.height = document.querySelector('.add-address-form form').scrollHeight + 'px'
    document.querySelector('.add-address-btn').addEventListener('click', function () {
        document.querySelector('.add-address-form form').classList.remove('closed')
        setTimeout(function () {
            document.querySelector('.add-address-form form').classList.add('overflow-visible')
        }, 300)
    })
    document.querySelectorAll('.edit-address-btn').forEach(function (item) {
        console.log(item.parentElement.parentElement.parentElement)
        item.addEventListener('click', function () {
            item.parentElement.parentElement.parentElement.parentElement.querySelector('form').classList.remove('closed')
            item.parentElement.parentElement.parentElement.parentElement.querySelector('.height-control-after-edit').classList.add('closed')
        })
    })
}

if (document.querySelector('.acc-info-container')) {
    document.querySelectorAll('.dashboard-acc__edit-btn').forEach(function (item) {
        item.addEventListener('click', function () {
            this.parentElement.parentElement.querySelector('input').removeAttribute('disabled')
        })
    })
}

const productSingleLightBox = GLightbox({});