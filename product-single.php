<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="">
<?php include 'header.php'; ?>
<div class="breadcrumb-container">
    <nav class="container" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">صفحه اصلی</a></li>
            <li class="breadcrumb-item"><a href="#">فروشگاه</a></li>
            <li class="breadcrumb-item"><a href="#">لوازم منزل و فانتزی</a></li>
            <li class="breadcrumb-item"><a href="#">لوازم شخصی</a></li>
            <li class="breadcrumb-item active" aria-current="page">هندزفری پلاتینا مدل</li>
        </ol>
    </nav>
</div>
<main>
    <section class="container mt-5 mt-lg-0">
        <div class="row">
            <div class="product-single__slider-container col-lg-6 pe-lg-0">
                <div class="swiper product-single-slider">
                    <div class="swiper-wrapper">
                        <a href="assets/images/product-single-slide.png" class="glightbox swiper-slide"
                           data-zoomable="true">
                            <img data-src="assets/images/product-single-slide.png"
                                 class="img-fluid w-100 js-lazy">
                        </a>
                        <a href="assets/images/product-single-slide.png" class="glightbox swiper-slide"
                           data-zoomable="true">
                            <img data-src="assets/images/product-single-slide.png"
                                 class="img-fluid w-100 js-lazy">
                        </a>
                        <a href="assets/images/product-single-slide.png" class="glightbox swiper-slide"
                           data-zoomable="true">
                            <img data-src="assets/images/product-single-slide.png"
                                 class="img-fluid w-100 js-lazy">
                        </a>
                        <a href="assets/images/product-single-slide.png" class="glightbox swiper-slide"
                           data-zoomable="true">
                            <img data-src="assets/images/product-single-slide.png"
                                 class="img-fluid w-100 js-lazy">
                        </a>
                        <a href="assets/images/slide1img.png" class="glightbox swiper-slide" data-zoomable="true">
                            <img data-src="assets/images/slide1img.png"
                                 class="img-fluid w-100 js-lazy">
                        </a>
                    </div>
                    <div class="product-single__slider__timer-container">
                        <div class="d-inline-flex flex-column justify-content-center align-items-center">
                            <div class="d-flex align-items-center justify-content-center w-100">
                                <div class="special-offer-time special-offer__minutes" data-time="54">
                                    <div>۰</div>
                                    <div>۰</div>
                                </div>
                                <img src="assets/images/clock-dots.svg" alt="">
                                <div class="special-offer-time special-offer__hours" data-time="16">
                                    <div>۰</div>
                                    <div>۰</div>
                                </div>
                                <img src="assets/images/clock-dots.svg" alt="">
                                <div class="special-offer-time special-offer__days" data-time="3">
                                    <div>۰</div>
                                    <div>۰</div>
                                </div>
                            </div>
                            <div class="d-flex w-100 special-offer__values-container">
                                <div>دقیقه</div>
                                <div>ساعت</div>
                                <div>روز</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper product-single-slider-thumb">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide"><img data-src="assets/images/product-single-slide.png"
                                                       class="img-fluid w-100 js-lazy">
                        </div>
                        <div class="swiper-slide"><img data-src="assets/images/product-single-slide.png"
                                                       class="img-fluid w-100 js-lazy">
                        </div>
                        <div class="swiper-slide"><img data-src="assets/images/product-single-slide.png"
                                                       class="img-fluid w-100 js-lazy">
                        </div>
                        <div class="swiper-slide"><img data-src="assets/images/product-single-slide.png"
                                                       class="img-fluid w-100 js-lazy">
                        </div>
                        <div class="swiper-slide"><img data-src="assets/images/slide1img.png"
                                                       class="img-fluid w-100 js-lazy">
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-info col-lg-6 ps-lg-5">
                <div class="product-info__title d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <span>هندزفری پلاتینا مدل PH-02</span>
                        <div class="product-info__off-badge">
                            ۲۰-
                        </div>
                    </div>
                    <div class="rater" data-rating="3"></div>
                </div>
                <div class="product-info__price">
                    <span>۷۱,۸۰۰</span>
                    <span>تومان</span>
                    <del>80,0۰۰</del>
                    <span>تومان</span>
                </div>
                <div class="product-info__props">
                    <div class="d-flex align-items-center justify-content-start">
                        <span>برند:</span>
                        <a href="#" class="product-info__brand-container">
                            <img data-src="assets/images/product-brand.png" class="img-fluid js-lazy">
                        </a>
                    </div>
                    <ul class="product-info__props-list">
                        <li>
                            نوع اتصال: بی‌سیم
                        </li>
                        <li>
                            نوع گوشی: دو گوشی
                        </li>
                        <li>
                            مناسب برای: مکالمه، گیمینگ، ورزش، کاربری عمومی
                        </li>
                        <li>
                            رابط: بی سیم
                        </li>
                        <li>
                            باتری: دارد
                        </li>
                    </ul>
                </div>
                <div class="d-flex align-items-center text-nowrap flex-wrap">
                    <button class="buy-now-btn mt-3">
                        <img data-src="assets/images/cards.svg" class="js-lazy me-1">
                        همین حالا بخرید
                    </button>
                    <button class="add-to-basket-btn mt-3">
                        <img data-src="assets/images/shopping-bag.svg" class="js-lazy me-1">
                        افزودن به سبد خرید
                    </button>
                    <div class="product-single__product-counter input-number-w-controllers mt-3">
                        <button class="input-inc"><img src="assets/images/plus.svg"></button>
                        <input type="number" value="1">
                        <button class="input-dec"><img src="assets/images/minus.svg"></button>
                    </div>
                </div>
                <div class="mt-4 my-2 my-md-4">
                    <button class="compare-btn">
                        <img data-src="assets/images/comparison.Svg" class="js-lazy">
                        مقایسه
                    </button>
                    <button class="add-to-fav-btn mt-3">
                        <img data-src="assets/images/heart-in-circle.svg" class="js-lazy">
                        افزودن به علاقه‌مندی‌ها
                    </button>
                </div>
                <div class="category-links">
                    دسته بندی:
                    <a href="">اکسسوری موبایل</a>،
                    <a href="">لوارم شخصی</a>،
                    <a href="">لوازم منزل و فانتزی</a>
                </div>
                <div class="tags-links">
                    دسته بندی:
                    <a href="">تگ</a>،
                    <a href="">تگ ۱</a>،
                    <a href="">تگ ۲</a>
                </div>
            </div>
            <div class="col-lg-6 mt-5">
                <div class="product-info__headers-container">
                    <button data-target="video" class="active">توضیحات محصول</button>
                    <button data-target="props-table">مشخصات</button>
                    <button data-target="comments">دیدگاه‌ها</button>
                </div>
                <div id="video" class="">
                    <img data-src="assets/images/product-single-video.png" class="img-fluid w-100 js-lazy">
                    <div class="mt-5">
                        <span>نقد و بررسی اجمالی</span>
                        <p>کاور 360 درجه از سه تکه تشکیل شده، بخش اصلی که پشت و کناره ها را پوشش میدهد از جنس PC و پلی
                            کربنات بوده و سخت است. دو بخش دیگر نیز از جنس پلی کربنات با کیفیت است که برای آسیب نرساندن
                            به
                            قاب گوشی کمی انعطاف پذیر نیز هست. در اطراف قسمت دوربین نیز برآمدگی تعبیه شده که از دوربین
                            نیز تا
                            حدودی محافظت شود. قسمت پورت ها، دوربین ها، سنسورها، اسپیکر ها و کلیدهای عملیاتی جلوی صفحه،
                            به
                            خوبی برش خورده و دسترسی به آن ها به راحتی امکانپذیر است. قسمت کلیدهای کناری نیز پوشش داده
                            شده و
                            قسمت منعطفی روی آن قرار گرفته که استفاده از کلیدها در عین محافظت از آنها راحت باشد. این قاب
                            بسیار سبک است و باعث افزایش وزن و ضخامت گوشی نمی شود.</p>
                    </div>
                </div>
                <div id="props-table" class="d-none">
                    <table class="product-single__props-table">
                        <tr>
                            <td>نوع اتصال</td>
                            <td>بی‌سیم</td>
                        </tr>
                        <tr>
                            <td>نوع گوشی</td>
                            <td>تو گوشی</td>
                        </tr>
                        <tr>
                            <td>رابط</td>
                            <td>جک ۳.۵ میلیمتری</td>
                        </tr>
                    </table>
                </div>
                <div id="comments" class="d-none">
                    <div class="product-single__comment">
                        <div class="product-single__comment__profile d-flex align-items-center justify-content-start">
                            <div class="product-single__comment__profile__img-container">
                                <img data-src="assets/images/profile-pic.png"
                                     class="product-single__comment__profile__img js-lazy">
                            </div>
                            <div>
                                <span class="fw-bold">امیر ناظری</span>
                                <br>
                                <span>1 ساعت پیش</span>
                            </div>
                        </div>
                        <div class="product-single__comment__text">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است
                        </div>
                    </div>
                    <div class="product-single__comment">
                        <div class="product-single__comment__profile d-flex align-items-center justify-content-start">
                            <div class="product-single__comment__profile__img-container">
                                <img data-src="assets/images/profile-pic.png"
                                     class="product-single__comment__profile__img js-lazy">
                            </div>
                            <div>
                                <span class="fw-bold">امیر ناظری</span>
                                <br>
                                <span>1 ساعت پیش</span>
                            </div>
                        </div>
                        <div class="product-single__comment__text">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است
                        </div>
                    </div>
                    <div class="product-single__comment">
                        <div class="product-single__comment__profile d-flex align-items-center justify-content-start">
                            <div class="product-single__comment__profile__img-container">
                                <img data-src="assets/images/profile-pic.png"
                                     class="product-single__comment__profile__img js-lazy">
                            </div>
                            <div>
                                <span class="fw-bold">امیر ناظری</span>
                                <br>
                                <span>1 ساعت پیش</span>
                            </div>
                        </div>
                        <div class="product-single__comment__text">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است
                        </div>
                    </div>
                    <div class="write-comment-container">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-flex align-items-center">
                                <img data-src="assets/images/no-pic-profile.svg" class="js-lazy">
                                <span>دیدگاهی دارید؟ بنویسید</span>
                            </div>
                            <div class="rater" data-read-only="false"></div>
                        </div>
                        <textarea name="" id="" cols="30" rows="10" placeholder="نظر خود را بنویسید..."></textarea>
                        <button>ارسال دیدگاه</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="related-products-section">
        <div class="container">
            <div class="related-products__header">
                <span class="instant-offer__heading">محصولات مرتبط</span>
            </div>
            <div class="related-products__carts-container">
                <div class="my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide2img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide1img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart off-active">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide3img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="my-cart">
                    <a href="#">
                        <div class="my-cart__off">۲۰٪</div>
                        <div class="my-cart__img-container">
                            <img data-src="assets/images/slide2img.png" class="img-fluid js-lazy">
                        </div>
                        <div class="my-cart__footer">
                            <span>هدفون بی‌ سیم هایلو مدل GT1</span>
                            <div class="my-cart__footer__price">
                                <span>۷۱,۸۰۰</span>
                                <span>تومن</span>
                                <del>۱۱۰.۰۰۰</del>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </section>
</main>
<?php include 'footer.php'; ?>
</body>
</html>