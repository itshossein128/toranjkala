<footer>
    <div class="d-flex align-items-center justify-content-between flex-column flex-md-row container">
        <div class="d-flex align-items-center justify-content-start">
            <img data-src="assets/images/dollar.svg" class="me-3 js-lazy">
            <div>
                <span>قیمت‌های مناسب</span>
                <br>
                <span>ارزان‌تر از همه جا</span>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-start">
            <img data-src="assets/images/dollar.svg" class="me-3 js-lazy">
            <div>
                <span>قیمت‌های مناسب</span>
                <br>
                <span>ارزان‌تر از همه جا</span>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-start">
            <img data-src="assets/images/dollar.svg" class="me-3 js-lazy">
            <div>
                <span>قیمت‌های مناسب</span>
                <br>
                <span>ارزان‌تر از همه جا</span>
            </div>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="">
                <img data-src="assets/images/logo-white.svg" class="js-lazy">
                <br>
                <span class="d-block mt-3 font-12">فروشگاه آنلاین ترنج کالا با هدف فروش محصولات مختلف در دسته‌بندی‌ های تلفن های هوشمند، لوازم جانبی و ... فعالیت خود را آغاز نموده است.</span>
                <br>
                <div class="counseling-container">
                    <img data-src="assets/images/footer-bg.png" class="footer-bg img-fluid w-100 js-lazy">
                    <div class="position-absolute d-flex align-items-center justify-content-start">
                        <img data-src="assets/images/24-hours.svg" class="me-2 js-lazy">
                        <div class="">
                            <span class="d-block">مشاوره پیش از خرید</span>
                            <a href="021-66128075">021-66128075</a>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <span class="footer__ul__heading">دسته‌بندی‌ها</span>
                <ul class="footer__ul-2-cols">
                    <li><a href="#">لوازم جانبی موبایل</a></li>
                    <li><a href="#">لوازم جانبی موبایل</a></li>
                    <li><a href="#">لوازم جانبی موبایل</a></li>
                    <li><a href="#">لوازم جانبی موبایل</a></li>
                    <li><a href="#">لوازم جانبی موبایل</a></li>
                    <li><a href="#">لوازم جانبی موبایل</a></li>
                    <li><a href="#">لوازم جانبی موبایل</a></li>
                    <li><a href="#">لوازم جانبی موبایل</a></li>
                </ul>
            </div>
            <div>
                <span class="footer__ul__heading">دیگر صفحات</span>
                <ul>
                    <li><a href="#">خانه</a></li>
                    <li><a href="#">خانه</a></li>
                    <li><a href="#">خانه</a></li>
                    <li><a href="#">همکاری در فروش</a></li>
                </ul>
            </div>
            <div>
                <span class="footer__ul__heading">خدمات مشتریان</span>
                <ul>
                    <li><a href="#">خانه</a></li>
                    <li><a href="#">خانه</a></li>
                    <li><a href="#">خانه</a></li>
                    <li><a href="#">قوانین و مقررات</a></li>
                </ul>
            </div>
            <div>
                <div class="nemad-container mb-3">
                    <img data-src="assets/images/nemad.png" class="js-lazy img-fluid w-100">
                </div>
                <div class="neshan-container">
                    <img data-src="assets/images/neshan.png" class="js-lazy img-fluid w-100">
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="d-flex flex-column flex-sm-row align-items-center align-items-sm-start justify-content-between py-4">
                <div class="location">
                    <img data-src="assets/images/location.svg" class="me-2 d-none d-sm-block js-lazy">
                    <span class="text-center text-sm-start">شیراز، کارگر شمالی خیابان نصرت نرسیده به جمالزاده کوچه بنده ای پلاک 15 واحد 1</span>
                </div>
                <div class="social-media d-flex align-items-center justify-content-center justify-content-sm-end">
                    <img data-src="assets/images/youtube.svg" class="ms-sm-3 js-lazy">
                    <img data-src="assets/images/pinterest.svg" class="ms-3 js-lazy">
                    <img data-src="assets/images/instagram.svg" class="ms-3 js-lazy">
                    <img data-src="assets/images/twitter.svg" class="ms-3 js-lazy">
                    <img data-src="assets/images/facebook.svg" class="ms-3 js-lazy">
                </div>
            </div>
            <div class="d-flex flex-column flex-md-row align-items-start justify-content-between">
                <span class="m-auto m-md-0">ساعات پاسخگویی: شنبه تا چهارشنبه ۹ الی ۱۸:۰۰ / پنجشنبه ۹ الی ۱۷:۰۰</span>
                <span class="m-auto m-md-0">کلیه حقوق برای ترنج کالا محفوظ است - 1401</span>
            </div>
        </div>
    </div>
</footer>
<!-- build:js -->
<script src="js/glightbox.min.js"></script>
<script src="js/lazyload.min.js"></script>
<script src="js/swiper.js"></script>
<script src="js/rater.js"></script>
<script src="js/main.js"></script>
<!-- endbuild -->