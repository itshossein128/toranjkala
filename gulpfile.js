import gulp from 'gulp';
import concat from 'gulp-concat';
import cleanCss from 'gulp-clean-css';
import uglify from 'gulp-uglify';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import purgeCss from 'gulp-purgecss';
import htmlreplace from 'gulp-html-replace';

const sass = gulpSass(dartSass);


let jsSources = [
    'js/lazyload.min.js',
    'js/glightbox.min.js',
    'js/swiper.js',
    'js/rater.js',
    'js/main.js'
]
let sassSource = ['sass/style.scss']
let imagesSource = ['assets/images/*']
let htmlSource = ['*.php']
let fontSources = ['assets/fonts/*']

gulp.task('js', function (done) {
    gulp.src(jsSources)
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
    done()
})
gulp.task("sass", function (done) {
    gulp.src(sassSource)
        .pipe(sass({style: 'expanded'}))
        .pipe(purgeCss({
            content: ['*.php', 'js/*.js'],
        }))
        .pipe(cleanCss({level: {1: {specialComments: 0}}}))
        .pipe(gulp.dest('dist'))
    done()
});
gulp.task('images', function (done) {
    gulp.src(imagesSource)
        .pipe(gulp.dest('dist/assets/images'));
    done()
})
gulp.task('html', function (done) {
    gulp.src(htmlSource)
        .pipe(htmlreplace({
            'css': 'style.css',
            'js': 'js/scripts.js'
        }))
        .pipe(gulp.dest('dist'));
    done()
})
gulp.task('fonts', function (done) {
    gulp.src(fontSources)
        .pipe(gulp.dest('dist/assets/fonts'));
    done()
})

gulp.task('watch', function () {
    gulp.watch(jsSources, gulp.series('js'));
    gulp.watch('style.css', gulp.series('sass'));
    gulp.watch(imagesSource, gulp.series('images'));
    gulp.watch(htmlSource, gulp.series('html'));
})


//because we don't need to watch task anymore:
// gulp.task('default', gulp.series('js', 'sass', 'images', 'html', 'fonts', 'watch'));
gulp.task('default', gulp.series('js', 'sass', 'images', 'html', 'fonts'));


//header v footer v main joda bashan va ba php be ham bechasban