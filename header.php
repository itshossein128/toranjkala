<div class="header-container">
    <div class="container">
        <nav class="navbar navbar-expand-lg py-0">
            <div class="d-flex flex-column w-100">
                <div class="header-first-part d-grid align-items-center">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_nav"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <img data-src="assets/images/navbar-toggler-icon.svg" class="js-lazy">
                    </button>
                    <a class="navbar-brand m-0" href="#"><img data-src="assets/images/logo.svg" class="js-lazy"></a>
                    <img src="assets/images/resselers.svg" class="resellers-badge">
                    <div class="header-search-container d-none d-lg-flex justify-content-start">
                        <form class="">
                            <input class="h-100" type="search" placeholder="جستجوی محصولات...">
                            <button class="h-100">
                                <img data-src="assets/images/search.svg" class="js-lazy">
                                <img data-src="assets/images/close-search.svg" class="js-lazy d-none">
                            </button>
                            <ul class="d-none">
                                <li><a class="d-flex align-self-center justify-content-start">
                                        <img src="assets/images/product1.png" class="me-2">
                                        <div class="d-flex flex-column">
                                            <span class="header-search-item-titre bold-text-observer">قاب موبایل</span>
                                            <span>۷۸،۰۰۰ تومان</span>
                                        </div>
                                    </a></li>
                                <li><a class="d-flex align-self-center justify-content-start">
                                        <img src="assets/images/product2.png" class="me-2">
                                        <div class="d-flex flex-column">
                                            <span class="header-search-item-titre bold-text-observer">قاب تبلت</span>
                                            <span>۷۸،۰۰۰ تومان</span>
                                        </div>
                                    </a></li>
                            </ul>
                        </form>
                    </div>
                    <div class="collapse navbar-collapse d-inline-flex justify-content-end" id="main_nav">
                        <ul class="navbar-nav">
                            <li class="nav-item text-nowrap ms-3 d-none d-lg-block"><a class="nav-link black-to-orange-container" href="#"><img
                                            data-src="assets/images/check.svg" class="js-lazy"></a></li>
                            <li class="nav-item text-nowrap ms-3 d-none d-lg-block"><a class="nav-link black-to-orange-container" href="#"><img
                                            data-src="assets/images/heart.svg" class="js-lazy"></a></li>
                            <li class="nav-item text-nowrap ms-0 ms-lg-3 dropdown shopping-cart">
                                <a class="nav-link dropdown-toggle position-relative black-to-orange-container" href="#"
                                   data-bs-toggle="dropdown">
                                    <img data-src="assets/images/shopping-cart.svg" class="js-lazy">
                                    <span class="shopping-cart-badge">2</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <ul class="p-0">
                                        <li class="d-flex align-items-start justify-content-between mx-3 py-3">
                                            <a class="dropdown-item d-flex align-items-start p-0" href="#">
                                                <img data-src="assets/images/product1.png" class="js-lazy">
                                                <div class="d-flex flex-column align-items-start">
                                                    <span class="shopping-cart__title">هدفون بی‌ سیم هایلو مدل GT1</span>
                                                    <span class="shopping-cart__price">۷۱,۸۰۰ تومان</span>
                                                </div>
                                            </a>
                                            <button class="shopping-cart__close-btn"><img
                                                        data-src="assets/images/close.svg"
                                                        class="js-lazy"></button>
                                        </li>
                                        <li class="d-flex align-items-start justify-content-between mx-3 py-3">
                                            <a class="dropdown-item d-flex align-items- p-0" href="#">
                                                <img data-src="assets/images/product1.png" class="js-lazy">
                                                <div class="d-flex flex-column align-items-start">
                                                    <span class="shopping-cart__title">هدفون بی‌ سیم هایلو مدل GT1</span>
                                                    <span class="shopping-cart__price">۷۱,۸۰۰ تومان</span>
                                                </div>
                                            </a>
                                            <button class="shopping-cart__close-btn"><img
                                                        data-src="assets/images/close.svg"
                                                        class="js-lazy"></button>
                                        </li>
                                    </ul>
                                    <div class="shopping-cart__links links mx-3 py-3 d-flex align-items-center justify-content-between">
                                        <a class="btn" href="#">
                                            <img data-src="assets/images/shopping-cart.svg" class="js-lazy">
                                            مشاهده سبد خرید
                                        </a>
                                        <a class="btn py-2 px-5" href="#">پرداخت</a>
                                    </div>
                                    <div class="shopping-cart__total-price mx-3 py-3 d-flex align-items-center justify-content-between">
                                        <span class="">مجموع</span>
                                        <span class="font-16">۷۱٫۸۰۰ <span class="font-12">تومان</span></span>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item text-nowrap nav__account-link ms-5 d-none d-lg-block">
                                <a class="nav-link" href="#">
                                    <img data-src="assets/images/account-icon.svg" class="me-1 js-lazy">
                                    حساب کاربری
                                </a>
                            </li>
                        </ul>
                    </div> <!-- navbar-collapse.// -->
                </div>
                <div class="d-none d-lg-flex align-items-center justify-content-between">
                    <ul class="navbar-nav">
                        <li class="nav-item me-3 dropdown navbar-nav__category-button">
                            <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">
                                <img data-src="assets/images/category-menu-icon.svg" class="js-lazy">
                                دسته‌بندی محصولات
                            </a>
                            <ul class="dropdown-menu w-100">
                                <li class="has-megasubmenu">
                                    <a class="dropdown-item" href="#"> موبایل </a>
                                    <div class="megasubmenu dropdown-menu">
                                        <div class="row m-0">
                                            <span class="megasubmenu__title">موبایل‌ها</span>
                                            <div class="col-6">
                                                <span class="title">برند</span>
                                                <ul class="list-unstyled">
                                                    <li><a class="d-block" href="#">اپل</a></li>
                                                    <li><a class="d-block" href="#">سامسونگ</a></li>
                                                    <li><a class="d-block" href="#">شیائومی</a></li>
                                                    <li><a class="d-block" href="#">هوآوی</a></li>
                                                </ul>
                                            </div><!-- end col-3 -->
                                            <div class="col-6">
                                                <span class="title">لب</span>
                                                <ul class="list-unstyled">
                                                    <li><a class="d-block" href="#">رژ لب</a></li>
                                                    <li><a class="d-block" href="#">بالم لب</a></li>
                                                    <li><a class="d-block" href="#">نرم و براق کننده لب</a></li>
                                                </ul>
                                            </div><!-- end col-3 -->
                                        </div><!-- end row -->
                                    </div>
                                </li>
                                <li><a class="dropdown-item" href="#">تبلت</a></li>
                                <li><a class="dropdown-item" href="#">قاب</a></li>
                                <li><a class="dropdown-item" href="#">هولدر</a></li>
                                <li class="has-megasubmenu">
                                    <a class="dropdown-item" href="#"> شارژر </a>
                                    <div class="megasubmenu dropdown-menu">
                                        <div class="row m-0">
                                            <span class="megasubmenu__title">موبایل‌ها</span>
                                            <div class="col-6">
                                                <span class="title">برند</span>
                                                <ul class="list-unstyled">
                                                    <li><a class="d-block" href="#">اپل</a></li>
                                                    <li><a class="d-block" href="#">سامسونگ</a></li>
                                                    <li><a class="d-block" href="#">شیائومی</a></li>
                                                    <li><a class="d-block" href="#">هوآوی</a></li>
                                                </ul>
                                            </div><!-- end col-3 -->
                                            <div class="col-6">
                                                <span class="title">لب</span>
                                                <ul class="list-unstyled">
                                                    <li><a class="d-block" href="#">رژ لب</a></li>
                                                    <li><a class="d-block" href="#">بالم لب</a></li>
                                                    <li><a class="d-block" href="#">نرم و براق کننده لب</a></li>
                                                </ul>
                                            </div><!-- end col-3 -->
                                        </div><!-- end row -->
                                    </div>
                                </li>
                                <li><a class="dropdown-item" href="#"> اسپیکر </a></li>
                                <li><a class="dropdown-item" href="#"> گلس </a></li>
                                <li><a class="dropdown-item" href="#"> فلش </a></li>
                            </ul>
                        </li>
                        <li class="nav-item me-3"><a class="nav-link" href="#"> مجله ترنج کالا </a></li>
                        <li class="nav-item me-3"><a class="nav-link" href="#"> برندها </a></li>
                        <li class="nav-item me-3"><a class="nav-link" href="#"> همکاری در فروش </a></li>
                    </ul>
                    <div class="dir-ltr nav-bar__contact-ways">
                        <a href="">
                            <img data-src="assets/images/sms.svg" class="ms-1 js-lazy">
                            toranjkala@gmail.com
                        </a>

                        <a href="">
                            <img data-src="assets/images/phone.svg" class="ms-1 js-lazy">
                            + 98(21) 1234 - 5678
                        </a>
                    </div>
                </div>
            </div> <!-- container-fluid.// -->
        </nav>
        <!-- ============= COMPONENT END// ============== -->
    </div>
</div>
<div class="shop-basket-mobile-size shopping-cart" style="display: none;">
    <div class="shop-basket-mobile-size-close">
        بستن
    </div>
    <ul class="p-0">
        <li class="d-flex align-items-start justify-content-between mx-3 py-3">
            <a class="dropdown-item d-flex align-items-start p-0" href="#">
                <img data-src="assets/images/product1.png" class="js-lazy">
                <div class="d-flex flex-column align-items-start">
                    <span class="shopping-cart__title">هدفون بی‌ سیم هایلو مدل GT1</span>
                    <span class="shopping-cart__price">۷۱,۸۰۰ تومان</span>
                </div>
            </a>
            <button class="shopping-cart__close-btn"><img data-src="assets/images/close.svg"
                                                          class="js-lazy"></button>
        </li>
        <li class="d-flex align-items-start justify-content-between mx-3 py-3">
            <a class="dropdown-item d-flex align-items- p-0" href="#">
                <img data-src="assets/images/product1.png" class="js-lazy">
                <div class="d-flex flex-column align-items-start">
                    <span class="shopping-cart__title">هدفون بی‌ سیم هایلو مدل GT1</span>
                    <span class="shopping-cart__price">۷۱,۸۰۰ تومان</span>
                </div>
            </a>
            <button class="shopping-cart__close-btn"><img data-src="assets/images/close.svg"
                                                          class="js-lazy"></button>
        </li>
    </ul>
    <div class="shopping-cart__links links mx-3 py-3 d-flex align-items-center justify-content-between">
        <a class="btn" href="#">
            <img data-src="assets/images/shopping-cart.svg" class="js-lazy">
            مشاهده سبد خرید
        </a>
        <a class="btn py-2 px-5" href="#">پرداخت</a>
    </div>
    <div class="shopping-cart__total-price mx-3 py-3 d-flex align-items-center justify-content-between">
        <span class="">مجموع</span>
        <span class="font-16">۷۱٫۸۰۰ <span class="font-12">تومان</span></span>
    </div>
</div>

<div class="mobile-menu" style="display: none;">
    <div class="mobile-menu-close">بستن</div>
    <div class="navbar-nav d-flex align-items-center justify-content-between flex-row px-2 py-4">
        <div class="d-flex align-items-center">
            <div class="nav-item text-nowrap ms-3 d-block d-lg-none"><a class="nav-link" href="#"><img
                            data-src="assets/images/check.svg" class="js-lazy"></a></div>
            <div class="nav-item text-nowrap ms-3 d-block d-lg-none"><a class="nav-link" href="#"><img
                            data-src="assets/images/heart.svg" class="js-lazy"></a></div>
        </div>
        <div class="nav-item text-nowrap nav__account-link ms-5 d-block d-lg-none">
            <a class="nav-link" href="#">
                <img data-src="assets/images/account-icon.svg" class="me-1 js-lazy">
                حساب کاربری
            </a>
        </div>
    </div>
    <div class="shop-filter-categories-container">
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 135px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
        <div class="shop-filter__category-closed">
            <button>
                <span>لوازم جانبی</span>
                <span class="shop-filter__number-of-products">۳۲</span>
                <div class="shop-filter-minus"></div>
            </button>
            <ul style="height: 134px;">
                <li><a href="#"><span>قاب</span><span>۵</span></a></li>
                <li><a href="#"><span>هولدر</span><span>۱۰</span></a></li>
                <li><a href="#"><span>شارژر</span><span>۳</span></a></li>
            </ul>
        </div>
    </div>
    <aside class="dashboard-aside">
        <div class="profile-container">
            <img class="js-lazy loaded" data-ll-status="loaded" src="assets/images/no-pic-profile.svg">
            <div class="d-flex flex-column align-items-start ms-2">
                <span>هومن حسنی</span>
                <span>۰۹۳۵۳۵۵۷۴۱۴</span>
            </div>
        </div>
        <ul>
            <li>
                <a href="#">
                    <img class="js-lazy loaded" data-ll-status="loaded" src="assets/images/dashboard.svg">
                    <img data-src="assets/images/dashboard-active.svg" class="dashboard-icon-active js-lazy">
                    داشبورد
                </a>
            </li>
            <li class="dashboard-active">
                <a href="#">
                    <img data-src="assets/images/orders.svg" class="js-lazy">
                    <img class="dashboard-icon-active js-lazy loaded" data-ll-status="loaded"
                         src="assets/images/orders-active.svg">
                    سفارش‌های من
                </a>
            </li>
            <li>
                <a href="#">
                    <img class="js-lazy loaded" data-ll-status="loaded" src="assets/images/dashboard-location.svg">
                    <img data-src="assets/images/dashboard-location-active.svg" class="dashboard-icon-active js-lazy">
                    نشانی‌ها
                </a>
            </li>
            <li>
                <a href="#">
                    <img class="js-lazy loaded" data-ll-status="loaded" src="assets/images/account.svg">
                    <img data-src="assets/images/account-active.svg" class="dashboard-icon-active js-lazy">
                    حساب کاربری
                </a>
            </li>
            <li>
                <a href="#" class="justify-content-between">
                    <div>
                        <img data-src="assets/images/tickets.svg" class="js-lazy">
                        <img data-src="assets/images/tickets-active.svg" class="dashboard-icon-active js-lazy">
                        تیکت‌ها
                    </div>
                    <span class="tickets-number">9</span>
                </a>
            </li>
            <li class="dashboard-li-separator"></li>
            <li>
                <a href="#">
                    <img class="js-lazy loaded" data-ll-status="loaded" src="assets/images/exit.svg">
                    <img data-src="assets/images/exit-active.svg" class="dashboard-icon-active js-lazy">
                    خروج
                </a>
            </li>
        </ul>
    </aside>
    <div class="dir-ltr nav-bar__contact-ways justify-content-center">
        <a href="">
            <img data-src="assets/images/sms.svg" class="ms-1 js-lazy">
            toranjkala@gmail.com
        </a>

        <a href="">
            <img data-src="assets/images/phone.svg" class="ms-1 js-lazy">
            + 98(21) 1234 - 5678
        </a>
    </div>
</div>
<div class="mobile-menu-shadow"></div>
<div class="mobile-shop-list-shadow"></div>