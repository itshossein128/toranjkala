<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-grey dashboard-page">
<?php include 'header.php'; ?>
<section class="dashboard-section container d-flex align-items-start justify-content-between">
    <aside class="dashboard-aside d-none d-lg-block">
        <div class="profile-container">
            <img data-src="assets/images/no-pic-profile.svg" class="js-lazy">
            <div class="d-flex flex-column align-items-start ms-2">
                <span>هومن حسنی</span>
                <span>۰۹۳۵۳۵۵۷۴۱۴</span>
            </div>
        </div>
        <ul>
            <li>
                <a href="#">
                    <img data-src="assets/images/dashboard.svg" class="js-lazy">
                    <img data-src="assets/images/dashboard-active.svg" class="dashboard-icon-active js-lazy">
                    داشبورد
                </a>
            </li>
            <li>
                <a href="#">
                    <img data-src="assets/images/orders.svg" class="js-lazy">
                    <img data-src="assets/images/orders-active.svg" class="dashboard-icon-active js-lazy">
                    سفارش‌های من
                </a>
            </li>
            <li>
                <a href="#">
                    <img data-src="assets/images/dashboard-location.svg" class="js-lazy">
                    <img data-src="assets/images/dashboard-location-active.svg" class="dashboard-icon-active js-lazy">
                    نشانی‌ها
                </a>
            </li>
            <li class="dashboard-active">
                <a href="#">
                    <img data-src="assets/images/account.svg" class="js-lazy">
                    <img data-src="assets/images/account-active.svg" class="dashboard-icon-active js-lazy">
                    حساب کاربری
                </a>
            </li>
            <li>
                <a href="#" class="justify-content-between">
                    <div>
                        <img data-src="assets/images/tickets.svg" class="js-lazy">
                        <img data-src="assets/images/tickets-active.svg" class="dashboard-icon-active js-lazy">
                        تیکت‌ها
                    </div>
                    <span class="tickets-number">9</span>
                </a>
            </li>
            <li class="dashboard-li-separator"></li>
            <li>
                <a href="#">
                    <img data-src="assets/images/exit.svg" class="js-lazy">
                    <img data-src="assets/images/exit-active.svg" class="dashboard-icon-active js-lazy">
                    خروج
                </a>
            </li>
        </ul>
    </aside>
    <div class="last-transaction">
        <span class="dashboard__heading">اطلاعات حساب</span>
        <div class="acc-info-container row">
            <div class="col-12 col-md-6">
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>نام و نام خانوادگی</span>
                        <input type="text" value="هومن حسنی" disabled>
                    </div>
                    <div>
                        <button class="btn dashboard-acc__edit-btn">
                            <img data-src="assets/images/edit.svg" class="js-lazy">
                        </button>
                    </div>
                </div>
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>تاریخ تولد</span>
                        <input type="text" value="18 شهریور 1372" disabled>
                    </div>
                    <div>
                        <button class="btn dashboard-acc__edit-btn">
                            <img data-src="assets/images/edit.svg" class="js-lazy">
                        </button>
                    </div>
                </div>
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>شماره همراه</span>
                        <input type="text" value="09353557414" disabled>
                    </div>
                    <div>
                        <button class="btn dashboard-acc__edit-btn">
                            <img data-src="assets/images/edit.svg" class="js-lazy">
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>کد ملی</span>
                        <input type="text" value="001-1234568" disabled>
                    </div>
                    <div>
                        <button class="btn dashboard-acc__edit-btn">
                            <img data-src="assets/images/edit.svg" class="js-lazy">
                        </button>
                    </div>
                </div>
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>ایمیل</span>
                        <input type="text" value="hooman.hassani@gmail.com" disabled>
                    </div>
                    <div>
                        <button class="btn dashboard-acc__edit-btn">
                            <img data-src="assets/images/edit.svg" class="js-lazy">
                        </button>
                    </div>
                </div>
                <div class="d-flex align-items-start justify-content-between">
                    <div class="d-flex flex-column align-items-start">
                        <span>رمز</span>
                        <input type="password" value="********" disabled>
                    </div>
                    <div>
                        <button class="btn dashboard-acc__edit-btn">
                            <img data-src="assets/images/edit.svg" class="js-lazy">
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php'; ?>
</body>
</html>