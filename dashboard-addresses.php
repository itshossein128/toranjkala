<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-grey dashboard-page">
<?php include 'header.php'; ?>
<section class="dashboard-section container d-flex align-items-start justify-content-between">
    <aside class="dashboard-aside d-none d-lg-block">
        <div class="profile-container">
            <img data-src="assets/images/no-pic-profile.svg" class="js-lazy">
            <div class="d-flex flex-column align-items-start ms-2">
                <span>هومن حسنی</span>
                <span>۰۹۳۵۳۵۵۷۴۱۴</span>
            </div>
        </div>
        <ul>
            <li>
                <a href="#">
                    <img data-src="assets/images/dashboard.svg" class="js-lazy">
                    <img data-src="assets/images/dashboard-active.svg" class="dashboard-icon-active js-lazy">
                    داشبورد
                </a>
            </li>
            <li>
                <a href="#">
                    <img data-src="assets/images/orders.svg" class="js-lazy">
                    <img data-src="assets/images/orders-active.svg" class="dashboard-icon-active js-lazy">
                    سفارش‌های من
                </a>
            </li>
            <li class="dashboard-active">
                <a href="#">
                    <img data-src="assets/images/dashboard-location.svg" class="js-lazy">
                    <img data-src="assets/images/dashboard-location-active.svg" class="dashboard-icon-active js-lazy">
                    نشانی‌ها
                </a>
            </li>
            <li>
                <a href="#">
                    <img data-src="assets/images/account.svg" class="js-lazy">
                    <img data-src="assets/images/account-active.svg" class="dashboard-icon-active js-lazy">
                    حساب کاربری
                </a>
            </li>
            <li>
                <a href="#" class="justify-content-between">
                    <div>
                        <img data-src="assets/images/tickets.svg" class="js-lazy">
                        <img data-src="assets/images/tickets-active.svg" class="dashboard-icon-active js-lazy">
                        تیکت‌ها
                    </div>
                    <span class="tickets-number">9</span>
                </a>
            </li>
            <li class="dashboard-li-separator"></li>
            <li>
                <a href="#">
                    <img data-src="assets/images/exit.svg" class="js-lazy">
                    <img data-src="assets/images/exit-active.svg" class="dashboard-icon-active js-lazy">
                    خروج
                </a>
            </li>
        </ul>
    </aside>
    <div class="last-transaction">
        <span class="dashboard__heading">نشانی‌ها</span>
        <div class="addresses-container">
            <div class="dashboard__address-container">
                <form class="closed">
                    <div class="row">
                        <div class="col-12">
                            <input type="text" class="form-control" placeholder="آدرس">
                        </div>
                        <div class="col-6">
                            <div class="custom-select-container">
                                <div class="custom-select">
                                    <select>
                                        <option value="1">استان</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="custom-select-container">
                                <div class="custom-select">
                                    <select>
                                        <option value="1">شهر</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="col-12">
                            <input type="text" class="form-control" placeholder="کد پستی">
                        </div>
                        <div class="col-12">
                            <input type="text" class="form-control" placeholder="شماره">
                        </div>
                    </div>
                    <div class="save-address-btn">
                        <button>ثبت اطلاعات</button>
                    </div>
                </form>
                <div class="height-control-after-edit d-flex flex-column">
                    <div class="d-flex align-items-start justify-content-between">
                        <span class="dashboard__address">خیابان آزادی، نرسیده به میدان انقلاب، کوچه کارگر، پلاک 50</span>
                        <div class="d-flex align-items-center">
                            <button class="btn edit-address-btn"><img data-src="assets/images/edit.svg" class="js-lazy">
                            </button>
                            <button class="address-erase btn"><img data-src="assets/images/erase.svg" class="js-lazy">
                            </button>
                        </div>
                    </div>
                    <span>تهران، تهران</span>
                    <span>۳۱۵۶۹۳۴۴۸۴</span>
                    <span>۰۹۳۵۳۵۵۷۴۱۴</span>
                </div>
            </div>
            <div class="dashboard__address-container">
                <form class="closed">
                    <div class="row">
                        <div class="col-12">
                            <input type="text" class="form-control" placeholder="آدرس">
                        </div>
                        <div class="col-6">
                            <div class="custom-select-container">
                                <div class="custom-select">
                                    <select>
                                        <option value="1">استان</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="custom-select-container">
                                <div class="custom-select">
                                    <select>
                                        <option value="1">شهر</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="col-12">
                            <input type="text" class="form-control" placeholder="کد پستی">
                        </div>
                        <div class="col-12">
                            <input type="text" class="form-control" placeholder="شماره">
                        </div>
                    </div>
                    <div class="save-address-btn">
                        <button>ثبت اطلاعات</button>
                    </div>
                </form>
                <div class="height-control-after-edit d-flex flex-column">
                    <div class="d-flex align-items-start justify-content-between">
                        <span class="dashboard__address">خیابان آزادی، نرسیده به میدان انقلاب، کوچه کارگر، پلاک 50</span>
                        <div class="d-flex align-items-center">
                            <button class="btn edit-address-btn"><img data-src="assets/images/edit.svg" class="js-lazy">
                            </button>
                            <button class="address-erase btn"><img data-src="assets/images/erase.svg" class="js-lazy">
                            </button>
                        </div>
                    </div>
                    <span>تهران، تهران</span>
                    <span>۳۱۵۶۹۳۴۴۸۴</span>
                    <span>۰۹۳۵۳۵۵۷۴۱۴</span>
                </div>
            </div>
            <div class="add-address-form">
                <form class="closed">
                    <div class="row">
                        <div class="col-12">
                            <input type="text" class="form-control" placeholder="آدرس">
                        </div>
                        <div class="col-6">
                            <div class="custom-select-container">
                                <div class="custom-select">
                                    <select>
                                        <option value="1">استان</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="custom-select-container">
                                <div class="custom-select">
                                    <select>
                                        <option value="1">شهر</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="col-12">
                            <input type="text" class="form-control" placeholder="کد پستی">
                        </div>
                        <div class="col-12">
                            <input type="text" class="form-control" placeholder="شماره">
                        </div>
                    </div>
                    <div class="save-address-btn">
                        <button>ثبت اطلاعات</button>
                    </div>
                </form>
                <button class="btn add-address-btn"><img data-src="assets/images/plus-in-circle.svg"
                                                         class="me-2 js-lazy"> افزودن آدرس
                </button>
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php'; ?>
</body>
</html>